#ifndef __STM8L051_H
#define __STM8L051_H

/**
 * @file
 * Header for STM8L051 value line.
 * Created from various example files. Please VERIFY before use.
 ******************************************************************************/


// Types
//-------------------------------------------------------------------------
typedef volatile unsigned char  hwbyte_t;
typedef volatile unsigned int   hwword_t;



// PORTS section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t ODR_D0             :1;   // 00.0   // Output Data Register, bit 0
        hwbyte_t ODR_D1             :1;   // 00.1   // Output Data Register, bit 1
        hwbyte_t ODR_D2             :1;   // 00.2   // Output Data Register, bit 2
        hwbyte_t ODR_D3             :1;   // 00.3   // Output Data Register, bit 3
        hwbyte_t ODR_D4             :1;   // 00.4   // Output Data Register, bit 4
        hwbyte_t ODR_D5             :1;   // 00.5   // Output Data Register, bit 5
        hwbyte_t ODR_D6             :1;   // 00.6   // Output Data Register, bit 6
        hwbyte_t ODR_D7             :1;   // 00.7   // Output Data Register, bit 7
        hwbyte_t IDR_D0             :1;   // 01.0   // Input Data Register, bit 0
        hwbyte_t IDR_D1             :1;   // 01.1   // Input Data Register, bit 1
        hwbyte_t IDR_D2             :1;   // 01.2   // Input Data Register, bit 2
        hwbyte_t IDR_D3             :1;   // 01.3   // Input Data Register, bit 3
        hwbyte_t IDR_D4             :1;   // 01.4   // Input Data Register, bit 4
        hwbyte_t IDR_D5             :1;   // 01.5   // Input Data Register, bit 5
        hwbyte_t IDR_D6             :1;   // 01.6   // Input Data Register, bit 6
        hwbyte_t IDR_D7             :1;   // 01.7   // Input Data Register, bit 7
        hwbyte_t DDR_D0             :1;   // 02.0   // Data Direction Register, bit 0
        hwbyte_t DDR_D1             :1;   // 02.1   // Data Direction Register, bit 1
        hwbyte_t DDR_D2             :1;   // 02.2   // Data Direction Register, bit 2
        hwbyte_t DDR_D3             :1;   // 02.3   // Data Direction Register, bit 3
        hwbyte_t DDR_D4             :1;   // 02.4   // Data Direction Register, bit 4
        hwbyte_t DDR_D5             :1;   // 02.5   // Data Direction Register, bit 5
        hwbyte_t DDR_D6             :1;   // 02.6   // Data Direction Register, bit 6
        hwbyte_t DDR_D7             :1;   // 02.7   // Data Direction Register, bit 7
        hwbyte_t CR1_D0             :1;   // 03.0   // Configuration Register 1, bit 0
        hwbyte_t CR1_D1             :1;   // 03.1   // Configuration Register 1, bit 1
        hwbyte_t CR1_D2             :1;   // 03.2   // Configuration Register 1, bit 2
        hwbyte_t CR1_D3             :1;   // 03.3   // Configuration Register 1, bit 3
        hwbyte_t CR1_D4             :1;   // 03.4   // Configuration Register 1, bit 4
        hwbyte_t CR1_D5             :1;   // 03.5   // Configuration Register 1, bit 5
        hwbyte_t CR1_D6             :1;   // 03.6   // Configuration Register 1, bit 6
        hwbyte_t CR1_D7             :1;   // 03.7   // Configuration Register 1, bit 7
        hwbyte_t CR2_D0             :1;   // 04.0   // Configuration Register 2, bit 0
        hwbyte_t CR2_D1             :1;   // 04.1   // Configuration Register 2, bit 1
        hwbyte_t CR2_D2             :1;   // 04.2   // Configuration Register 2, bit 2
        hwbyte_t CR2_D3             :1;   // 04.3   // Configuration Register 2, bit 3
        hwbyte_t CR2_D4             :1;   // 04.4   // Configuration Register 2, bit 4
        hwbyte_t CR2_D5             :1;   // 04.5   // Configuration Register 2, bit 5
        hwbyte_t CR2_D6             :1;   // 04.6   // Configuration Register 2, bit 6
        hwbyte_t CR2_D7             :1;   // 04.7   // Configuration Register 2, bit 7
    };
    struct {
        hwbyte_t ODR;                     // 00     // Output Data Register
        hwbyte_t IDR;                     // 01     // Input Data Register
        hwbyte_t DDR;                     // 02     // Data Direction Register
        hwbyte_t CR1;                     // 03     // Configuration Register 1
        hwbyte_t CR2;                     // 04     // Configuration Register 2
    };
} GPIO_t;
GPIO_t PA                           @0x5000;
GPIO_t PB                           @0x5005;
GPIO_t PC                           @0x500A;
GPIO_t PD                           @0x500F;



// FLASH section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CR1_FIX            :1;   // 00.0   // FIX programming time
        hwbyte_t CR1_IE             :1;   // 00.1   // flash Interrupt Enable
        hwbyte_t CR1_WAITM          :1;   // 00.2   // flash program and data EEPROM IDDQ mode during wait
        hwbyte_t CR1_EEPM           :1;   // 00.3   // flash program and data EEPROM IDDQ mode during run
        hwbyte_t                    :4;   // 00.4-7
        hwbyte_t CR2_PRG            :1;   // 01.0   // Standard block PRoGramming
        hwbyte_t                    :3;   // 01.1-3
        hwbyte_t CR2_FPRG           :1;   // 01.4   // Fast PRoGramming mode
        hwbyte_t CR2_ERASE          :1;   // 01.5   // ERASE block
        hwbyte_t CR2_WPRG           :1;   // 01.6   // Word PRoGramming
        hwbyte_t CR2_OPT            :1;   // 01.7   // select OPTion byte
        hwbyte_t                    :8;   // 02
        hwbyte_t                    :8;   // 03
        hwbyte_t IAPSR_WRPGDIS      :1;   // 04.0   // WRite attempted to Protected paGe
        hwbyte_t IAPSR_PUL          :1;   // 04.1   // Flash Program memory unlocked flag
        hwbyte_t IAPSR_EOP          :1;   // 04.2   // End of operation flag
        hwbyte_t IAPSR_DUL          :1;   // 04.3   // Data EEPROM unlocked flag
        hwbyte_t                    :2;   // 04.4-5
        hwbyte_t IAPSR_HVOFF        :1;   // 04.6   // end of High Voltage flag
        hwbyte_t                    :1;   // 04.7
    };
    struct {
        hwbyte_t CR1;                     // 00     // Control Register 1
        hwbyte_t CR2;                     // 01     // Control Register 2
        hwbyte_t PUKR;                    // 02     // Program memory Unprotection Key Register
        hwbyte_t DUKR;                    // 03     // Data EEPROM Unprotection Key Register
        hwbyte_t IAPSR;                   // 04     // In-Application Program Status Register
    };
} FLASH_t;
FLASH_t FLASH                       @0x5050;



// DMA section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t GCSR_GEN           :1;   // 00.0   // Global ENable
        hwbyte_t GCSR_GB            :1;   // 00.1   // Global Busy
        hwbyte_t GCSR_TO            :6;   // 00.2-7 // TimeOut
        hwbyte_t GIR1_IFC0          :1;   // 01.0   // Interrupt Flag Channel 0
        hwbyte_t GIR1_IFC1          :1;   // 01.1   // Interrupt Flag Channel 1
        hwbyte_t GIR1_IFC2          :1;   // 01.2   // Interrupt Flag Channel 2
        hwbyte_t GIR1_IFC3          :1;   // 01.3   // Interrupt Flag Channel 3
        hwbyte_t                    :4;   // 01.4-7
        hwbyte_t                    :8;   // 02
        hwbyte_t                    :8;   // 03
        hwbyte_t                    :8;   // 04
        hwbyte_t C0CR_EN            :1;   // 05.0   // Channel 0 ENable
        hwbyte_t C0CR_TCIE          :1;   // 05.1   // Channel 0 Transaction Complete Interrupt Enable
        hwbyte_t C0CR_HTIE          :1;   // 05.2   // Channel 0 Half-Transaction Interrupt Enable
        hwbyte_t C0CR_DIR           :1;   // 05.3   // Channel 0 data transfer DIRection
        hwbyte_t C0CR_CIRC          :1;   // 05.4   // Channel 0 CIRCular buffer
        hwbyte_t C0CR_MINCDEC       :1;   // 05.5   // Channel 0 Memory INCrement / DECrement mode
        hwbyte_t                    :1;   // 05.6
        hwbyte_t                    :1;   // 05.7
        hwbyte_t                    :1;   // 06.0
        hwbyte_t C0SPR_TCIF         :1;   // 06.1   // Channel 0 Transaction Complete Interrupt Flag
        hwbyte_t C0SPR_HTIF         :1;   // 06.2   // Channel 0 Half-Transaction Interrupt Flag
        hwbyte_t C0SPR_TSIZE        :1;   // 06.3   // Channel 0 Transfer SIZE
        hwbyte_t C0SPR_PL           :2;   // 06.4-5 // Channel 0 Priority Level
        hwbyte_t C0SPR_PEND         :1;   // 06.6   // Channel 0 PENDing
        hwbyte_t C0SPR_BUSY         :1;   // 06.7   // Channel 0 BUSY
        hwbyte_t                    :8;   // 07
        hwword_t C0PAR;                   // 08-09  // Channel 0 Peripheral Address Register
        hwbyte_t                    :8;   // 0A
        hwword_t C0M0AR;                  // 0B-0C  // Channel 0 Memory 0 Address Register
        hwbyte_t                    :8;   // 0D
        hwbyte_t                    :8;   // 0E
        hwbyte_t C1CR_EN            :1;   // 0F.0   // Channel 1 ENable
        hwbyte_t C1CR_TCIE          :1;   // 0F.1   // Channel 1 Transaction Complete Interrupt Enable
        hwbyte_t C1CR_HTIE          :1;   // 0F.2   // Channel 1 Half-Transaction Interrupt Enable
        hwbyte_t C1CR_DIR           :1;   // 0F.3   // Channel 1 data transfer DIRection
        hwbyte_t C1CR_CIRC          :1;   // 0F.4   // Channel 1 CIRCular buffer
        hwbyte_t C1CR_MINCDEC       :1;   // 0F.5   // Channel 1 Memory INCrement / DECrement mode
        hwbyte_t                    :1;   // 0F.6
        hwbyte_t                    :1;   // 0F.7
        hwbyte_t                    :1;   // 10.0
        hwbyte_t C1SPR_TCIF         :1;   // 10.1   // Channel 1 Transaction Complete Interrupt Flag
        hwbyte_t C1SPR_HTIF         :1;   // 10.2   // Channel 1 Half-Transaction Interrupt Flag
        hwbyte_t C1SPR_TSIZE        :1;   // 10.3   // Channel 1 Transfer SIZE
        hwbyte_t C1SPR_PL           :2;   // 10.4-5 // Channel 1 Priority Level
        hwbyte_t C1SPR_PEND         :1;   // 10.6   // Channel 1 PENDing
        hwbyte_t C1SPR_BUSY         :1;   // 10.7   // Channel 1 BUSY
        hwbyte_t                    :8;   // 11
        hwword_t C1PAR;                   // 12-13  // Channel 1 Peripheral Address Register
        hwbyte_t                    :8;   // 14
        hwword_t C1M0AR;                  // 15-16  // Channel 1 Memory 0 Address Register
        hwbyte_t                    :8;   // 17
        hwbyte_t                    :8;   // 18
        hwbyte_t C2CR_EN            :1;   // 19.0   // Channel 2 ENable
        hwbyte_t C2CR_TCIE          :1;   // 19.1   // Channel 2 Transaction Complete Interrupt Enable
        hwbyte_t C2CR_HTIE          :1;   // 19.2   // Channel 2 Half-Transaction Interrupt Enable
        hwbyte_t C2CR_DIR           :1;   // 19.3   // Channel 2 data transfer DIRection
        hwbyte_t C2CR_CIRC          :1;   // 19.4   // Channel 2 CIRCular buffer
        hwbyte_t C2CR_MINCDEC       :1;   // 19.5   // Channel 2 Memory INCrement / DECrement mode
        hwbyte_t                    :1;   // 19.6
        hwbyte_t                    :1;   // 19.7
        hwbyte_t                    :1;   // 1A.0
        hwbyte_t C2SPR_TCIF         :1;   // 1A.1   // Channel 2 Transaction Complete Interrupt Flag
        hwbyte_t C2SPR_HTIF         :1;   // 1A.2   // Channel 2 Half-Transaction Interrupt Flag
        hwbyte_t C2SPR_TSIZE        :1;   // 1A.3   // Channel 2 Transfer SIZE
        hwbyte_t C2SPR_PL           :2;   // 1A.4-5 // Channel 2 Priority Level
        hwbyte_t C2SPR_PEND         :1;   // 1A.6   // Channel 2 PENDing
        hwbyte_t C2SPR_BUSY         :1;   // 1A.7   // Channel 2 BUSY
        hwbyte_t                    :8;   // 1B
        hwword_t C2PAR;                   // 1C-1D  // Channel 2 Peripheral Address Register
        hwbyte_t                    :8;   // 1E
        hwword_t C2M0AR;                  // 1F-20  // Channel 2 Memory 0 Address Register
        hwbyte_t                    :8;   // 21
        hwbyte_t                    :8;   // 22
        hwbyte_t C3CR_EN            :1;   // 23.0   // Channel 3 ENable
        hwbyte_t C3CR_TCIE          :1;   // 23.1   // Channel 3 Transaction Complete Interrupt Enable
        hwbyte_t C3CR_HTIE          :1;   // 23.2   // Channel 3 Half-Transaction Interrupt Enable
        hwbyte_t C3CR_DIR           :1;   // 23.3   // Channel 3 data transfer DIRection
        hwbyte_t C3CR_CIRC          :1;   // 23.4   // Channel 3 CIRCular buffer
        hwbyte_t C3CR_MINCDEC       :1;   // 23.5   // Channel 3 Memory INCrement / DECrement mode
        hwbyte_t C3CR_MEM           :1;   // 23.6   // Channel 3 MEMory transfer enable
        hwbyte_t                    :1;   // 23.7
        hwbyte_t                    :1;   // 24.0
        hwbyte_t C3SPR_TCIF         :1;   // 24.1   // Channel 3 Transaction Complete Interrupt Flag
        hwbyte_t C3SPR_HTIF         :1;   // 24.2   // Channel 3 Half-Transaction Interrupt Flag
        hwbyte_t C3SPR_TSIZE        :1;   // 24.3   // Channel 3 Transfer SIZE
        hwbyte_t C3SPR_PL           :2;   // 24.4-5 // Channel 3 Priority Level
        hwbyte_t C3SPR_PEND         :1;   // 24.6   // Channel 3 PENDing
        hwbyte_t C3SPR_BUSY         :1;   // 24.7   // Channel 3 BUSY
        hwbyte_t                    :8;   // 25
        hwword_t C3PAR;                   // 26-27  // Channel 3 Peripheral Address Register
        hwbyte_t                    :8;   // 28
        hwword_t C3M0AR;                  // 29-2A  // Channel 3 Memory 0 Address Register
        hwbyte_t                    :8;   // 2B
        hwbyte_t                    :8;   // 2C
    };
    struct {
        hwbyte_t GCSR;                    // 00     // Global Configuration and Status Register
        hwbyte_t GIR1;                    // 01     // Global Interrupt Register 1
        hwbyte_t                    :8;   // 02
        hwbyte_t                    :8;   // 03
        hwbyte_t                    :8;   // 04
        hwbyte_t C0CR;                    // 05     // Channel 0 Configuration Register
        hwbyte_t C0SPR;                   // 06     // Channel 0 Status & Priority Register
        hwbyte_t C0NDTR;                  // 07     // Channel 0 Number of Data to Transfer Register
        hwbyte_t C0PARH;                  // 08     // Channel 0 Peripheral Address Register High
        hwbyte_t C0PARL;                  // 09     // Channel 0 Peripheral Address Register Low
        hwbyte_t                    :8;   // 0A
        hwbyte_t C0M0ARH;                 // 0B     // Channel 0 Memory 0 Address Register High
        hwbyte_t C0M0ARL;                 // 0C     // Channel 0 Memory 0 Address Register Low
        hwbyte_t                    :8;   // 0D
        hwbyte_t                    :8;   // 0E
        hwbyte_t C1CR;                    // 0F     // Channel 1 Configuration Register
        hwbyte_t C1SPR;                   // 10     // Channel 1 Status & Priority Register
        hwbyte_t C1NDTR;                  // 11     // Channel 1 Number of Data to Transfer Register
        hwbyte_t C1PARH;                  // 12     // Channel 1 Peripheral Address Register High
        hwbyte_t C1PARL;                  // 13     // Channel 1 Peripheral Address Register Low
        hwbyte_t                    :8;   // 14
        hwbyte_t C1M0ARH;                 // 15     // Channel 1 Memory 0 Address Register High
        hwbyte_t C1M0ARL;                 // 16     // Channel 1 Memory 0 Address Register Low
        hwbyte_t                    :8;   // 17
        hwbyte_t                    :8;   // 18
        hwbyte_t C2CR;                    // 19     // Channel 2 Configuration Register
        hwbyte_t C2SPR;                   // 1A     // Channel 2 Status & Priority Register
        hwbyte_t C2NDTR;                  // 1B     // Channel 2 Number of Data to Transfer Register
        hwbyte_t C2PARH;                  // 1C     // Channel 2 Peripheral Address Register High
        hwbyte_t C2PARL;                  // 1D     // Channel 2 Peripheral Address Register Low
        hwbyte_t                    :8;   // 1E
        hwbyte_t C2M0ARH;                 // 1F     // Channel 2 Memory 0 Address Register High
        hwbyte_t C2M0ARL;                 // 20     // Channel 2 Memory 0 Address Register Low
        hwbyte_t                    :8;   // 21
        hwbyte_t                    :8;   // 22
        hwbyte_t C3CR;                    // 23     // Channel 3 Configuration Register
        hwbyte_t C3SPR;                   // 24     // Channel 3 Status & Priority Register
        hwbyte_t C3NDTR;                  // 25     // Channel 3 Number of Data to Transfer Register
        hwbyte_t C3PARH;                  // 26     // Channel 3 Peripheral Address Register High
        hwbyte_t C3PARL;                  // 27     // Channel 3 Peripheral Address Register Low
        hwbyte_t                    :8;   // 28
        hwbyte_t C3M0ARH;                 // 29     // Channel 3 Memory 0 Address Register High
        hwbyte_t C3M0ARL;                 // 2A     // Channel 3 Memory 0 Address Register Low
        hwbyte_t                    :8;   // 2B
        hwbyte_t                    :8;   // 2C
    };
} DMA_t;
DMA_t DMA1                          @0x5070;
DMA_t DMA                           @0x5070;



// SYSCFG section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t                    :6;   // 00.0-5
        hwbyte_t RMPCR3_TIM2CH1     :1;   // 00.6   // TIM2 CHannel 1 mapping
        hwbyte_t RMPCR3_TIM2CH2     :1;   // 00.7   // TIM2 CHannel 2 mapping
        hwbyte_t RMPCR1_ADC1DMA     :2;   // 01.0-1 // ADC1 DMA channel mapping
        hwbyte_t RMPCR1_TIM4DMA     :2;   // 01.2-3 // TIM4 DMA channel mapping
        hwbyte_t RMPCR1_USART1TR    :2;   // 01.4-5 // USART1 Tx Rx mapping
        hwbyte_t RMPCR1_USART1CK    :1;   // 01.6   // USART1 CK mapping
        hwbyte_t RMPCR1_SPI1        :1;   // 01.7   // SPI1 mapping
        hwbyte_t RMPCR2_ADC1TRIG    :1;   // 02.0   // ADC1 TRIG mapping
        hwbyte_t RMPCR2_TIM2TRIG    :1;   // 02.1   // TIM2 TRIG mapping
        hwbyte_t RMPCR2_TIM3TRIG    :1;   // 02.2   // TIM3 TRIG mapping
        hwbyte_t RMPCR2_TIM2TRIGLSE :1;   // 02.3   // TIM2 TRIG LSE mapping
        hwbyte_t RMPCR2_TIM3TRIGLSE :1;   // 02.4   // TIM3 TRIG LSE mapping
        hwbyte_t                    :3;   // 02.5-7
    };
    struct {
        hwbyte_t RMPCR3;                  // 00     // ReMaPping Control Register 3
        hwbyte_t RMPCR1;                  // 01     // ReMaPping Control Register 1
        hwbyte_t RMPCR2;                  // 02     // ReMaPping Control Register 2
    };
} SYSCFG_t;
SYSCFG_t SYSCFG                     @0x509D;



// External Interrupt Controller section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CR1_P0IS           :2;   // 00.0-1 // Portx bit 0 external Interrupt Sensitivity
        hwbyte_t CR1_P1IS           :2;   // 00.2-3 // Portx bit 1 external Interrupt Sensitivity
        hwbyte_t CR1_P2IS           :2;   // 00.4-5 // Portx bit 2 external Interrupt Sensitivity
        hwbyte_t CR1_P3IS           :2;   // 00.6-7 // Portx bit 3 external Interrupt Sensitivity
        hwbyte_t CR2_P4IS           :2;   // 01.0-1 // Portx bit 4 external Interrupt Sensitivity
        hwbyte_t CR2_P5IS           :2;   // 01.2-3 // Portx bit 5 external Interrupt Sensitivity
        hwbyte_t CR2_P6IS           :2;   // 01.4-5 // Portx bit 6 external Interrupt Sensitivity
        hwbyte_t CR2_P7IS           :2;   // 01.6-7 // Portx bit 7 external Interrupt Sensitivity
        hwbyte_t CR2_PBIS           :2;   // 02.0-1 // PortB external Interrupt Sensitivity
        hwbyte_t CR2_PDIS           :2;   // 02.2-3 // PortD external Interrupt Sensitivity
        hwbyte_t                    :4;   // 02.4-7
        hwbyte_t SR1_P0F            :1;   // 03.0   // Portx bit 0 external interrupt Flag
        hwbyte_t SR1_P1F            :1;   // 03.1   // Portx bit 1 external interrupt Flag
        hwbyte_t SR1_P2F            :1;   // 03.2   // Portx bit 2 external interrupt Flag
        hwbyte_t SR1_P3F            :1;   // 03.3   // Portx bit 3 external interrupt Flag
        hwbyte_t SR1_P4F            :1;   // 03.4   // Portx bit 4 external interrupt Flag
        hwbyte_t SR1_P5F            :1;   // 03.5   // Portx bit 5 external interrupt Flag
        hwbyte_t SR1_P6F            :1;   // 03.6   // Portx bit 6 external interrupt Flag
        hwbyte_t SR1_P7F            :1;   // 03.7   // Portx bit 7 external interrupt Flag
        hwbyte_t SR2_PBF            :1;   // 04.0   // PortB external interrupt Flag
        hwbyte_t SR2_PDF            :1;   // 04.1   // PortD external interrupt Flag
        hwbyte_t                    :6;   // 04.2-7
        hwbyte_t CONF1_PBLIS        :1;   // 05.0   // PortB bits 0-3 external interrupt select
        hwbyte_t CONF1_PBHIS        :1;   // 05.1   // PortB bits 4-7 external interrupt select
        hwbyte_t CONF1_PDLIS        :1;   // 05.2   // PortD bit 0 external interrupt select
        hwbyte_t                    :5;   // 05.3-7
    };
    struct {
        hwbyte_t CR1;                     // 00     // Control Register 1
        hwbyte_t CR2;                     // 01     // Control Register 2
        hwbyte_t CR3;                     // 02     // Control Register 2
        hwbyte_t SR1;                     // 03     // Status Register 1
        hwbyte_t SR2;                     // 04     // Status Register 2
        hwbyte_t CONF1;                   // 05     // CONFiguration Register 1
    };
} EXTI_t;
EXTI_t EXTI                         @0x50A0;



// Wait for event section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CR1_TIM2EV0        :1;   // 00.0   // TIM2 interrupt EVent 0 (update, trigger, break)
        hwbyte_t CR1_TIM2EV1        :1;   // 00.1   // TIM2 interrupt EVent 1 (capture, compare)
        hwbyte_t                    :2;   // 00.2-3
        hwbyte_t CR1_EXTIEV0        :1;   // 00.4   // Portx bit 0 external interrupt EVent
        hwbyte_t CR1_EXTIEV1        :1;   // 00.5   // Portx bit 1 external interrupt EVent
        hwbyte_t CR1_EXTIEV2        :1;   // 00.6   // Portx bit 2 external interrupt EVent
        hwbyte_t CR1_EXTIEV3        :1;   // 00.7   // Portx bit 3 external interrupt EVent
        hwbyte_t CR2_EXTIEV4        :1;   // 01.0   // Portx bit 0 external interrupt EVent
        hwbyte_t CR2_EXTIEV5        :1;   // 01.1   // Portx bit 0 external interrupt EVent
        hwbyte_t CR2_EXTIEV6        :1;   // 01.2   // Portx bit 0 external interrupt EVent
        hwbyte_t CR2_EXTIEV7        :1;   // 01.3   // Portx bit 0 external interrupt EVent
        hwbyte_t CR2_EXTIEVB        :1;   // 01.4   // PortB external interrupt EVent
        hwbyte_t CR2_EXTIEVD        :1;   // 01.5   // PortD external interrupt EVent
        hwbyte_t                    :1;   // 01.6
        hwbyte_t CR2_ADC1EV         :1;   // 01.7   // ADC1 interrupt EVent
        hwbyte_t CR3_TIM3EV0        :1;   // 02.0   // TIM3 interrupt EVent 0 (update, trigger, break)
        hwbyte_t CR3_TIM3EV1        :1;   // 02.1   // TIM3 interrupt EVent 1 (capture, compare)
        hwbyte_t CR3_TIM4EV         :1;   // 02.2   // TIM4 interrupt EVent 0 (update, trigger)
        hwbyte_t CR3_SPI1EV         :1;   // 02.3   // SPI1 rx or tx interrupt EVent
        hwbyte_t CR3_I2C1EV         :1;   // 02.4   // I2C1 rx or tx interrupt EVent
        hwbyte_t CR3_USART1EV       :1;   // 02.5   // USART rx or tx interrupt EVent
        hwbyte_t CR3_DMA1CH01EV     :1;   // 02.6   // DMA1 CHannel 0 or 1 interrupt EVent
        hwbyte_t CR3_DMA1CH23EV     :1;   // 02.7   // DMA1 CHannel 2 or 3 interrupt EVent
        hwbyte_t CR4_RTCCSSLSEEV    :1;   // 03.0   // RTC or CSS on LSE EVent
        hwbyte_t                    :7;   // 03.1-7
    };
    struct {
        hwbyte_t CR1;                     // 00     // Control Register 1
        hwbyte_t CR2;                     // 01     // Control Register 2
        hwbyte_t CR3;                     // 02     // Control Register 3
        hwbyte_t CR4;                     // 03     // Control Register 4
    };
} WFE_t;
WFE_t WFE                           @0x50A6;



// Reset section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t                    :8;   // 00
        hwbyte_t SR_PORF            :1;   // 01.0   // Power On Reset Flag
        hwbyte_t SR_IWDGF           :1;   // 01.1   // Independent WatchDoG Reset Flag
        hwbyte_t SR_ILLOPF          :1;   // 01.2   // ILLegal OPcode Reset Flag
        hwbyte_t SR_SWIMF           :1;   // 01.3   // SWIM reset Flag
        hwbyte_t SR_WWDGF           :1;   // 01.4   // Windowed WatchDoG Reset Flag
        hwbyte_t SR_BORF            :1;   // 01.5   // BrownOut Reset Flag
        hwbyte_t                    :2;   // 01.6-7
    };
    struct {
        hwbyte_t CR;                      // 00     // Control Register
        hwbyte_t SR;                      // 01     // Status Register
    };
} RST_t;
RST_t RST                           @0x50B0;



// Power section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CSR1_PVDE          :1;   // 00.0   // Power Voltage Detector Enable
        hwbyte_t CSR1_PLS           :3;   // 00.1-3 // Power voltage detector Level Selection
        hwbyte_t CSR1_PVDIEN        :1;   // 00.4   // Power Voltage Detector Interrupt Enable
        hwbyte_t CSR1_PVDIF         :1;   // 00.5   // Power Voltage Detector Interrupt Flag
        hwbyte_t CSR1_PVDOF         :1;   // 00.6   // Power Voltage Detector Output Flag
        hwbyte_t                    :1;   // 00.7
        hwbyte_t CSR2_VREFINTF      :1;   // 01.0   // Voltage REFerence INTernal status Flag
        hwbyte_t CSR2_ULP           :1;   // 01.1   // Ultra Low Power configuration
        hwbyte_t CSR2_FWU           :1;   // 01.2   // Fast Wake-Up configuration
        hwbyte_t                    :5;   // 01.3-7
    };
    struct {
        hwbyte_t CSR1;                    // 00     // Control and Status Register 1
        hwbyte_t CSR2;                    // 01     // Control and Status Register 2
    };
} PWR_t;
PWR_t PWR                           @0x50B2;



// Clock section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CKDIVR_CKM         :3;   // 00.0-2 // system ClocK prescaler
        hwbyte_t                    :5;   // 00.3-7
        hwbyte_t CRTCR_RTCSWBSY     :1;   // 01.0   // RTC clock SWitch: system BuSY
        hwbyte_t CRTCR_RTCSEL       :4;   // 01.1-4 // RTC clock SELection
        hwbyte_t CRTCR_RTCDIV       :3;   // 01.5-7 // RTC clock DIVider
        hwbyte_t ICKCR_HSION        :1;   // 02.0   // High Speed Internal Oscillator eNable
        hwbyte_t ICKCR_HSIRDY       :1;   // 02.1   // High Speed internal oscillator ReaDY
        hwbyte_t ICKCR_LSION        :1;   // 02.2   // Low Speed Internal Oscillator eNable
        hwbyte_t ICKCR_LSIRDY       :1;   // 02.3   // Low Speed Internal oscillator ReaDY
        hwbyte_t ICKCR_SAHALT       :1;   // 02.4   // Slow Active HALT mode
        hwbyte_t ICKCR_FHWU         :1;   // 02.5   // Fast Wake-Up from active halt/Halt mode
        hwbyte_t ICKCR_BEEPAHALT    :1;   // 02.6   // BEEP clock (Active) HALT mode
        hwbyte_t                    :1;   // 02.7
        hwbyte_t PCKENR1_TIM2       :1;   // 03.0   // TIMer 2 clock enable
        hwbyte_t PCKENR1_TIM3       :1;   // 03.1   // TIMer 3 clock enable
        hwbyte_t PCKENR1_TIM4       :1;   // 03.2   // TIMer 4 clock enable
        hwbyte_t PCKENR1_I2C1       :1;   // 03.3   // I2C1 clock enable
        hwbyte_t PCKENR1_SPI1       :1;   // 03.4   // SPI1 clock enable
        hwbyte_t PCKENR1_USART1     :1;   // 03.5   // USART1 clock enable
        hwbyte_t PCKENR1_BEEP       :1;   // 03.6   // BEEP clock enable
        hwbyte_t                    :1;   // 03.7
        hwbyte_t PCKENR2_ADC1       :1;   // 04.0   // ADC1 clock enable
        hwbyte_t                    :1;   // 04.1
        hwbyte_t PCKENR2_RTC        :1;   // 04.2   // RTC clock enable
        hwbyte_t                    :1;   // 04.3
        hwbyte_t PCKENR2_DMA1       :1;   // 04.4   // DMA1 clock enable
        hwbyte_t                    :2;   // 04.5-6
        hwbyte_t PCKENR2_BOOTROM    :1;   // 04.7   // BOOT ROM clock enable
        hwbyte_t CCOR_CCOSWBSY      :1;   // 05.0   // Configurable Clock Output SWitch BuSY
        hwbyte_t CCOR_CCOSEL        :4;   // 05.1-4 // Configurable Clock Output SELection
        hwbyte_t CCOR_CCODIV        :3;   // 05.5-7 // Configurable Clock Output DIVider
        hwbyte_t ECKCR_HSEON        :1;   // 06.0   // High Speed External crystal Oscillator eNable
        hwbyte_t ECKCR_HSERDY       :1;   // 06.1   // High Speed External crystal oscillator ReaDY
        hwbyte_t ECKCR_LSEON        :1;   // 06.2   // Low Speed External crystal Oscillator eNable
        hwbyte_t ECKCR_LSERDY       :1;   // 06.3   // Low Speed External crystal oscillator ReaDY
        hwbyte_t ECKCR_HSEBYP       :1;   // 06.0   // High Speed External clock BYPass
        hwbyte_t ECKCR_LSEBYP       :1;   // 06.0   // Low Speed External clock BYPass
        hwbyte_t                    :2;   // 06.6-7
        hwbyte_t SCSR_HSI           :1;   // 07.0   // High Speed Internal clock is used currently
        hwbyte_t SCSR_LSI           :1;   // 07.1   // Low Speed Internal clock is used currently
        hwbyte_t SCSR_HSE           :1;   // 07.2   // High Speed External clock is used currently
        hwbyte_t SCSR_LSE           :1;   // 07.3   // Low Speed External clock is used currently
        hwbyte_t                    :4;   // 07.4-7
        hwbyte_t SWR_SWIHSI         :1;   // 08.0   // System clock selection bit 0 (HSI)
        hwbyte_t SWR_SWILSI         :1;   // 08.1   // System clock selection bit 1 (LSI)
        hwbyte_t SWR_SWIHSE         :1;   // 08.2   // System clock selection bit 2 (HSE)
        hwbyte_t SWR_SWILSE         :1;   // 08.3   // System clock selection bit 3 (LSE)
        hwbyte_t                    :4;   // 08.4-7
        hwbyte_t SWCR_SWBSY         :1;   // 09.0   // SWitch BuSY
        hwbyte_t SWCR_SWEN          :1;   // 09.1   // SWitch start / stop ENable
        hwbyte_t SWCR_SWIEN         :1;   // 09.2   // SWitch Interrupt ENable
        hwbyte_t SWCR_SWIF          :1;   // 09.3   // SWitch Interrupt Flag
        hwbyte_t                    :4;   // 09.4-7
        hwbyte_t CSSR_CSSEN         :1;   // 0A.0   // Clock Security System ENable
        hwbyte_t CSSR_AUX           :1;   // 0A.1   // AUXiliary oscillator connected to system clock
        hwbyte_t CSSR_CSSDIE        :1;   // 0A.2   // Clock Security System Detection Interrupt Enable
        hwbyte_t CSSR_CSSD          :1;   // 0A.3   // Clock Security System Detection
        hwbyte_t CSSR_CSSDGON       :1;   // 0A.4   // Clock Security System DeGlicher ON
        hwbyte_t                    :3;   // 0A.5-7
        hwbyte_t CBEEPR_BEEPSWBSY   :1;   // 0B.0   // BEEP clock SWitch: system BuSY
        hwbyte_t CBEEPR_CLKBEEPSEL  :2;   // 0B.1-2 // BEEP CLocK SELection
        hwbyte_t                    :5;   // 0B.3-7
        hwbyte_t                    :8;   // 0C
        hwbyte_t                    :8;   // 0D
        hwbyte_t                    :8;   // 0E
        hwbyte_t REGCSR_REGRDY      :1;   // 0F.0   // main REGulator ReaDY
        hwbyte_t REGCSR_REGOFF      :1;   // 0F.1   // main REGulator OFF
        hwbyte_t REGCSR_HSIPD       :1;   // 0F.2   // High Speed Internal oscillator Power-Down
        hwbyte_t REGCSR_LSIPD       :1;   // 0F.3   // Low Speed Internal oscillator Power-Down
        hwbyte_t REGCSR_HSEPD       :1;   // 0F.4   // High Speed External oscillator Power-Down
        hwbyte_t REGCSR_LSEPD       :1;   // 0F.5   // Low Speed External oscillator Power-Down
        hwbyte_t REGCSR_EEBSY       :1;   // 0F.6   // Flash program and data EEPROM BuSY
        hwbyte_t REGCSR_EERDY       :1;   // 0F.7   // Flash program and data EEPROM ReaDY
        hwbyte_t                    :5;   // 10.0-4
        hwbyte_t PCKENR3_CSSLSE     :1;   // 10.5   // CSS on LSE clock enable
        hwbyte_t                    :2;   // 10.6-7
    };
    struct {
        hwbyte_t CKDIVR;                  // 00     // ClocK DIVider Register
        hwbyte_t CRTCR;                   // 01     // Clock Real Time Clock Register
        hwbyte_t ICKCR;                   // 02     // Internal ClocK Control Register
        hwbyte_t PCKENR1;                 // 03     // Peripheral ClocK ENable Register 1
        hwbyte_t PCKENR2;                 // 04     // Peripheral ClocK ENable Register 2
        hwbyte_t CCOR;                    // 05     // Configurable Clock Output Register
        hwbyte_t ECKCR;                   // 06     // External ClocK Control Register
        hwbyte_t SCSR;                    // 07     // System Clock Status Register
        hwbyte_t SWR;                     // 08     // master SWitch Register
        hwbyte_t SWCR;                    // 09     // SWitch Control Register
        hwbyte_t CSSR;                    // 0A     // Clock Security System Register
        hwbyte_t CBEEPR;                  // 0B     // Clock BEEP Register
        hwbyte_t HSICALR;                 // 0C     // High Speed Internal CALibration Register
        hwbyte_t HSITRIMR;                // 0D     // High Speed Internal TRIMming Register
        hwbyte_t HSIUNLCKR;               // 0E     // High Speed Internal UNLock Register
        hwbyte_t REGCSR;                  // 0F     // clock REGulator Control Status Register
        hwbyte_t PCKENR3;                 // 10     // Peripheral ClocK ENable Register 3
    };
} CLK_t;
CLK_t CLK                         @0x50C0;



// Windowed Watchdog section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t                    :6;   // 00.0-5 // Control Register
        hwbyte_t CR_T6              :1;   // 00.6   // counter T6 bit
        hwbyte_t CR_WDGA            :1;   // 00.7   // WatchDoG Activation
        hwbyte_t WR_W               :7;   // 01.0-6 // Window
        hwbyte_t                    :1;   // 01.7
    };
    struct {
        hwbyte_t CR_T               :7;   // 00.0-6 // counter
        hwbyte_t                    :1;   // 00.7
        hwbyte_t                    :8;   // 01
    };
    struct {
        hwbyte_t CR;                      // 00     // Control Register
        hwbyte_t WR;                      // 01     // Window Register
    };
} WWDG_t;
WWDG_t WWDG                         @0x50D3;



// Independent Watchdog section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t KR;                      // 00     // Key Register
        hwbyte_t PR;                      // 01     // Prescaler Register
        hwbyte_t RLR;                     // 02     // ReLoad Register
    };
} IWDG_t;
IWDG_t IWDG                         @0x50E0;



// BEEP section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CSR1_MSR           :1;   // 00.0   // MeaSuRement enable
        hwbyte_t                    :7;   // 00.1-7
        hwbyte_t                    :8;   // 01
        hwbyte_t                    :8;   // 02
        hwbyte_t CSR2_BEEPDIV       :5;   // 03.0-4 // BEEP prescaler DIVider
        hwbyte_t CSR2_BEEPEN        :1;   // 03.5   // BEEP ENable
        hwbyte_t CSR2_BEEPSEL       :2;   // 03.6-7 // BEEP SELection
    };
    struct {
        hwbyte_t CSR1;                    // 00     // Control Status Register 1
        hwbyte_t                    :8;   // 01
        hwbyte_t                    :8;   // 02
        hwbyte_t CSR2;                    // 03     // Control Status Register 2
    };
} BEEP_t;
BEEP_t BEEP                         @0x50F0;



// Real time clock section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t TR1_SU             :4;   // 00.0-3 // Second Units BCD
        hwbyte_t TR1_ST             :3;   // 00.4-6 // Second Tens BCD
        hwbyte_t                    :1;   // 00.7
        hwbyte_t TR2_MNU            :4;   // 01.0-3 // Minute Units BCD
        hwbyte_t TR2_MNT            :3;   // 01.4-6 // Minute Tens BCD
        hwbyte_t                    :1;   // 01.7
        hwbyte_t TR3_HU             :4;   // 02.0-3 // Hour Units BCD
        hwbyte_t TR3_HT             :2;   // 02.4-5 // Hour Tens BCD
        hwbyte_t TR3_PM             :1;   // 02.6   // PM in 12-hour format
        hwbyte_t                    :1;   // 02.7
        hwbyte_t                    :8;   // 03
        hwbyte_t DR1_DU             :4;   // 04.0-3 // Day Units BCD
        hwbyte_t DR1_DT             :2;   // 04.4-5 // Day Tens BCD
        hwbyte_t                    :2;   // 04.6-7
        hwbyte_t DR2_MU             :4;   // 05.0-3 // Month Units BCD
        hwbyte_t DR2_MT             :1;   // 05.4   // Month Tens BCD
        hwbyte_t DR2_WDU            :3;   // 05.5-7 // WeekDay Units
        hwbyte_t DR3_YU             :4;   // 06.0-3 // Year Units BCD
        hwbyte_t DR3_YT             :4;   // 06.4-7 // Year Tens BCD
        hwbyte_t                    :8;   // 07
        hwbyte_t CR1_WUCKSEL        :3;   // 08.0-2 // Wake-Up ClocK SELection
        hwbyte_t                    :1;   // 08.3
        hwbyte_t CR1_BYPSHAD        :1;   // 08.4   // BYPass SHADow registers
        hwbyte_t CR1_RATIO          :1;   // 08.5   // system clock RTC clock RATIO
        hwbyte_t CR1_FMT            :1;   // 08.6   // hour ForMaT
        hwbyte_t                    :1;   // 08.7
        hwbyte_t CR2_ALRAE          :1;   // 09.0   // Wake-Up ClocK SELection
        hwbyte_t                    :1;   // 09.1
        hwbyte_t CR2_WUTE           :1;   // 09.2   // Wake-Up Timer Enable
        hwbyte_t                    :1;   // 09.3
        hwbyte_t CR2_ALRAIE         :1;   // 09.4   // ALaRm A Interrupt Enable
        hwbyte_t                    :1;   // 09.5
        hwbyte_t CR2_WUTIE          :1;   // 09.6   // Wake-Up Timer Interrupt Enable
        hwbyte_t                    :1;   // 09.7
        hwbyte_t CR3_ADD1H          :1;   // 0A.0   // ADD 1 Hour
        hwbyte_t CR3_SUB1H          :1;   // 0A.1   // SUBstract 1 Hour
        hwbyte_t CR3_BCK            :1;   // 0A.2   // BaCKup
        hwbyte_t CR3_COSEL          :1;   // 0A.3   // Calibration Output SELection
        hwbyte_t CR3_POL            :1;   // 0A.4   // output POLarity
        hwbyte_t CR3_OSEL           :2;   // 0A.5-6 // Output SELection
        hwbyte_t CR3_COE            :1;   // 0A.7   // Calibration Output Enable
        hwbyte_t                    :8;   // 0B
        hwbyte_t ISR1_ALRAWF        :1;   // 0C.0   // ALaRm A Write Flag
        hwbyte_t ISR1_RECALPF       :1;   // 0C.1   // RECALibration Pending Flag
        hwbyte_t ISR1_WUTWF         :1;   // 0C.2   // Wake-Up Timer Write Flag
        hwbyte_t ISR1_SHPF          :1;   // 0C.3   // SHift operation Pending flag
        hwbyte_t ISR1_INITS         :1;   // 0C.4   // INITialization Status flag
        hwbyte_t ISR1_RSF           :1;   // 0C.5   // Register Synchronization Flag
        hwbyte_t ISR1_INITF         :1;   // 0C.6   // INITialization Flag
        hwbyte_t ISR1_INIT          :1;   // 0C.7   // INITialization
        hwbyte_t ISR2_ALRAF         :1;   // 0D.0   // ALaRm A Flag
        hwbyte_t                    :1;   // 0D.1
        hwbyte_t ISR2_WUTF          :1;   // 0D.2   // periodic Wake-Up Flag
        hwbyte_t                    :2;   // 0D.3-4
        hwbyte_t ISR2_TAMP1F        :1;   // 0D.5   // TAMPer 1 detection flag
        hwbyte_t ISR2_TAMP2F        :1;   // 0D.6   // TAMPer 2 detection flag
        hwbyte_t ISR2_TAMP3F        :1;   // 0D.7   // TAMPer 3 detection flag
        hwbyte_t                    :8;   // 0E
        hwbyte_t                    :8;   // 0F
        hwword_t SPRER;                   // 10-11  // Synchronous PREscaler Register
        hwbyte_t                    :8;   // 12
        hwbyte_t                    :8;   // 13
        hwword_t WUTR;                    // 14-15  // WakeUp Timer Register
        hwbyte_t                    :8;   // 16
        hwword_t SSR;                     // 17-18  // SubSecond Register (swapped!!!)
        hwbyte_t                    :8;   // 19
        hwword_t SHIFTR;                  // 1A-1B  // SHIFT Register
        hwbyte_t ALRMAR1_SU         :4;   // 1C.0-3 // ALaRm A Second Units BCD
        hwbyte_t ALRMAR1_ST         :3;   // 1C.4-6 // ALaRm A Second Tens BCD
        hwbyte_t ALRMAR1_MSK1       :1;   // 1C.7   // ALaRm A second MaSK
        hwbyte_t ALRMAR2_MNU        :4;   // 1D.0-3 // ALaRm A Minute Units BCD
        hwbyte_t ALRMAR2_MNT        :3;   // 1D.4-6 // ALaRm A Minute Tens BCD
        hwbyte_t ALRMAR2_MSK2       :1;   // 1D.7   // ALaRm A minute MaSK
        hwbyte_t ALRMAR3_HU         :4;   // 1E.0-3 // ALaRm A Hour Units BCD
        hwbyte_t ALRMAR3_HT         :2;   // 1E.4-5 // ALaRm A Hour Tens BCD
        hwbyte_t ALRMAR3_PM         :1;   // 1E.6   // ALaRm A PM in 12-hour format
        hwbyte_t ALRMAR3_MSK3       :1;   // 1E.7   // ALaRm A hour MaSK
        hwbyte_t ALRMAR4_DU         :4;   // 1F.0-3 // ALaRm A Day Units or weekday BCD
        hwbyte_t ALRMAR4_DT         :2;   // 1F.4-5 // ALaRm A Day Tens BCD
        hwbyte_t ALRMAR4_WDSEL      :1;   // 1F.6   // ALaRm A WeekDay SELection
        hwbyte_t ALRMAR4_MSK4       :1;   // 1F.7   // ALaRm A date MaSK
        hwbyte_t                    :8;   // 20
        hwbyte_t                    :8;   // 21
        hwbyte_t                    :8;   // 22
        hwbyte_t                    :8;   // 23
        hwword_t ALRMASSR;                // 24-25  // ALaRm A SubSecond Register
        hwbyte_t ALRMASSMSKR_MSKSS  :4;   // 26.0-3 // ALaRm A MaSK bits of SubSecond Register
        hwbyte_t                    :4;   // 26.4-7
        hwbyte_t                    :8;   // 27
        hwbyte_t                    :8;   // 28
        hwbyte_t                    :8;   // 29
        hwbyte_t CALRH_CALM8        :1;   // 2A.0   // CALibration Minus bit 8
        hwbyte_t                    :4;   // 2A.1-4
        hwbyte_t CALRH_CALW16       :1;   // 2A.5   // CALibration cycle 16 seconds
        hwbyte_t CALRH_CALW8        :1;   // 2A.6   // CALibration cycle 8 seconds
        hwbyte_t CALRH_CALP         :1;   // 2A.7   // CALibration Plus
        hwbyte_t CALRL_CALM         :8;   // 2B.0-7 // CALibration Minus bits 0-7
        hwbyte_t TCR1_TAMPIE        :1;   // 2C.0   // TAMper Interrupt Enable
        hwbyte_t TCR1_TAMP1E        :1;   // 2C.1   // TAMper 1 detection Enable
        hwbyte_t TCR1_TAMP1TRG      :1;   // 2C.2   // TAMper 1 active level for TRiGgering
        hwbyte_t TCR1_TAMP2E        :1;   // 2C.0   // TAMper 2 detection Enable
        hwbyte_t TCR1_TAMP2TRG      :1;   // 2C.4   // TAMper 2 active level for TRiGgering
        hwbyte_t TCR1_TAMP3E        :1;   // 2C.5   // TAMper 3 detection Enable
        hwbyte_t TCR1_TAMP3TRG      :1;   // 2C.6   // TAMper 3 active level for TRiGgering
        hwbyte_t                    :1;   // 2C.7
        hwbyte_t TCR2_TAMPFREQ      :3;   // 2D.0-2 // TAMper sampling FREQuency
        hwbyte_t TCR2_TAMPFLT       :2;   // 2D.3-4 // TAMper FiLTer count
        hwbyte_t TCR2_TAMPPRCH      :2;   // 2D.5-6 // TAMper PReCHarge duration
        hwbyte_t TCR2_TAMPPUDIS     :1;   // 2D.7   // TAMper Pull-Up DISable
    };
    struct {
        hwbyte_t TR1;                     // 00     // Time Register 1
        hwbyte_t TR2;                     // 01     // Time Register 2
        hwbyte_t TR3;                     // 02     // Time Register 3
        hwbyte_t                    :8;   // 03
        hwbyte_t DR1;                     // 04     // Date Register 1
        hwbyte_t DR2;                     // 05     // Date Register 2
        hwbyte_t DR3;                     // 06     // Date Register 3
        hwbyte_t                    :8;   // 07
        hwbyte_t CR1;                     // 08     // Control Register 1
        hwbyte_t CR2;                     // 09     // Control Register 2
        hwbyte_t CR3;                     // 0A     // Control Register 3
        hwbyte_t                    :8;   // 0B
        hwbyte_t ISR1;                    // 0C     // Init and Status Register 1
        hwbyte_t ISR2;                    // 0D     // Init and Status Register 2
        hwbyte_t                    :8;   // 0E
        hwbyte_t                    :8;   // 0F
        hwbyte_t SPRERH;                  // 10     // Synchronous PREscaler Register High
        hwbyte_t SPRERL;                  // 11     // Synchronous PREscaler Register Low
        hwbyte_t APRER;                   // 12     // Asynchronous PREscaler Register
        hwbyte_t                    :8;   // 13
        hwbyte_t WUTRH;                   // 14     // WakeUp Timer Register High
        hwbyte_t WUTRL;                   // 15     // WakeUp Timer Register Low
        hwbyte_t                    :8;   // 16
        hwbyte_t SSRL;                    // 17     // SubSecond Register Low
        hwbyte_t SSRH;                    // 18     // SubSecond Register High
        hwbyte_t WPR;                     // 19     // Write Protection Register
        hwbyte_t SHIFTRH;                 // 1A     // SHIFT Register High
        hwbyte_t SHIFTRL;                 // 1B     // SHIFT Register Low
        hwbyte_t ALRMAR1;                 // 1C     // ALaRm A Register 1
        hwbyte_t ALRMAR2;                 // 1D     // ALaRm A Register 2
        hwbyte_t ALRMAR3;                 // 1E     // ALaRm A Register 3
        hwbyte_t ALRMAR4;                 // 1F     // ALaRm A Register 4
        hwbyte_t                    :8;   // 20
        hwbyte_t                    :8;   // 21
        hwbyte_t                    :8;   // 22
        hwbyte_t                    :8;   // 23
        hwbyte_t ALRMASSRH;               // 24     // ALaRM A SubSecond Register High
        hwbyte_t ALRMASSRL;               // 25     // ALaRM A SubSecond Register Low
        hwbyte_t ALRMASSMSKR;             // 26     // ALaRm A SubSecond MaSKing Register
        hwbyte_t                    :8;   // 27
        hwbyte_t                    :8;   // 28
        hwbyte_t                    :8;   // 29
        hwbyte_t CALRH;                   // 2A     // CALibration Register High
        hwbyte_t CALRL;                   // 2B     // CALibration Register Low
        hwbyte_t TCR1;                    // 2C     // Tamper Control Register 1
        hwbyte_t TCR2;                    // 2D     // Tamper Control Register 2
    };
} RTC_t;
RTC_t RTC                           @0x5140;



// CSS on LSE section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CSR_CSSEN          :1;   // 00.0   // Clock Security System on LSE enable
        hwbyte_t CSR_SWITCHEN       :1;   // 00.1   // RTC clock SWITCH to LSI in case of LSE failure
        hwbyte_t CSR_CSSIE          :1;   // 00.2   // Clock Security System Interrupt Enable
        hwbyte_t CSR_CSSF           :1;   // 00.3   // Clock Security System Flag
        hwbyte_t CSR_SWITCHF        :1;   // 00.4   // RTC clock SWITCH to LSI Flag
        hwbyte_t                    :3;   // 00.5-7
    };
    struct {
        hwbyte_t CSR;                     // 00     // Control Status Register
    };
} CSSLSE_t;
CSSLSE_t CSSLSE                     @0x5190;



//  SPI section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CR1_CPHA           :1;   // 00.0   // Clock PHAse
        hwbyte_t CR1_CPOL           :1;   // 00.1   // Clock POLarity
        hwbyte_t CR1_MSTR           :1;   // 00.2   // MaSTer selection
        hwbyte_t CR1_BR             :3;   // 00.3-5 // Baud Rate control
        hwbyte_t CR1_SPE            :1;   // 00.6   // SPI Enable
        hwbyte_t CR1_LSBFIRST       :1;   // 00.7   // frame format
        hwbyte_t CR2_SSI            :1;   // 01.0   // Internal Slave Select
        hwbyte_t CR2_SSM            :1;   // 01.1   // Software Slave Management
        hwbyte_t CR2_RXONLY         :1;   // 01.2   // RX ONLY
        hwbyte_t                    :1;   // 01.3
        hwbyte_t CR2_CRCNEXT        :1;   // 01.4   // Transmit CRC NEXT
        hwbyte_t CR2_CRCEN          :1;   // 01.5   // CRC calculation ENable
        hwbyte_t CR2_BDOE           :1;   // 01.6   // Bi-Directional mode Output Enable
        hwbyte_t CR2_BDM            :1;   // 01.7   // Bi-Directional mode enable
        hwbyte_t ICR_RXDMAEN        :1;   // 02.0   // RX buffer DMA ENable
        hwbyte_t ICR_TXDMAEN        :1;   // 02.1   // TX buffer DMA ENable
        hwbyte_t                    :2;   // 02.2-3
        hwbyte_t ICR_WKIE           :1;   // 02.4   // WaKe-up Interrupt Enable
        hwbyte_t ICR_ERRIE          :1;   // 02.5   // ERRor Interrupt Enable
        hwbyte_t ICR_RXEI           :1;   // 02.6   // RX buffer empty Interrupt Enable
        hwbyte_t ICR_TXEI           :1;   // 02.7   // TX buffer empty Interrupt Enable
        hwbyte_t SR_RXNE            :1;   // 03.0   // RX buffer Not Empty
        hwbyte_t SR_TXE             :1;   // 03.1   // TX buffer Empty
        hwbyte_t                    :1;   // 03.2
        hwbyte_t SR_WKUP            :1;   // 03.3   // WaKe-UP flag
        hwbyte_t SR_CRCERR          :1;   // 03.4   // CRC ERRor flag
        hwbyte_t SR_MODF            :1;   // 03.5   // MODe Fault
        hwbyte_t SR_OVR             :1;   // 03.6   // OVerRun flag
        hwbyte_t SR_BSY             :1;   // 03.7   // BuSY flag
        hwbyte_t                    :8;   // 04
        hwbyte_t                    :8;   // 05
        hwbyte_t                    :8;   // 06
        hwbyte_t                    :8;   // 07
    };
    struct {
        hwbyte_t CR1;                     // 00     // Control Register 1
        hwbyte_t CR2;                     // 01     // Control Register 2
        hwbyte_t ICR;                     // 02     // Interrupt Control Register
        hwbyte_t SR;                      // 03     // Status Register
        hwbyte_t DR;                      // 04     // Data Register
        hwbyte_t CRCPR;                   // 05     // CRC Polynomial Register
        hwbyte_t RXCRCR;                  // 06     // RX CRC Register
        hwbyte_t TXCRCR;                  // 07     // TX CRC Register
    };
} SPI_t;
SPI_t SPI1                          @0x5200;
SPI_t SPI                           @0x5200;



// I2C section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CR1_PE             :1;   // 00.0   // Peripheral Enable
        hwbyte_t CR1_SMBUS          :1;   // 00.1   // SMBUS mode
        hwbyte_t                    :1;   // 00.2
        hwbyte_t CR1_SMBTYPE        :1;   // 00.1   // SMBus TYPE
        hwbyte_t CR1_ENARP          :1;   // 00.1   // ENable ARP
        hwbyte_t CR1_ENPEC          :1;   // 00.1   // ENable Packet Error Checking calculation
        hwbyte_t CR1_ENGC           :1;   // 00.6   // ENable General Call
        hwbyte_t CR1_NOSTRETCH      :1;   // 00.7   // NO clock STRETCHing in slave mode
        hwbyte_t CR2_START          :1;   // 01.0   // START generation
        hwbyte_t CR2_STOP           :1;   // 01.1   // STOP generation
        hwbyte_t CR2_ACK            :1;   // 01.2   // ACKnowledge enable
        hwbyte_t CR2_POS            :1;   // 01.3   // acknowledge POSition
        hwbyte_t CR2_PEC            :1;   // 01.4   // Packet Error Checking
        hwbyte_t CR2_ALERT          :1;   // 01.5   // SMBUS ALERT
        hwbyte_t                    :1;   // 01.6
        hwbyte_t CR2_SWRST          :1;   // 01.7   // SoftWare RESet
        hwbyte_t FREQR_FREQ         :6;   // 02.0-5 // peripheral clock FREQuency
        hwbyte_t                    :2;   // 02.6-7
        hwbyte_t                    :8;   // 03
        hwbyte_t                    :1;   // 04.0
        hwbyte_t OARH_ADD           :2;   // 04.1-2 // ADDress bits [9..8] in 10-bit addressing mode
        hwbyte_t                    :3;   // 04.3-5
        hwbyte_t OARH_ADDCONF       :1;   // 04.6   // ADDress mode CONFiguration
        hwbyte_t OARH_ADDMODE       :1;   // 04.7   // ADDressing MODE in slave mode
        hwbyte_t                    :8;   // 05
        hwbyte_t                    :8;   // 06
        hwbyte_t SR1_SB             :1;   // 07.0   // Start Bit in master mode
        hwbyte_t SR1_ADDR           :1;   // 07.1   // ADDRess sent in master mode, matched in slave mode
        hwbyte_t SR1_BTF            :1;   // 07.2   // Byte Transfer Finished
        hwbyte_t SR1_ADD10          :1;   // 07.3   // 10-bit ADDress header sent in master mode
        hwbyte_t SR1_STOPF          :1;   // 07.4   // STOP detected Flag
        hwbyte_t                    :1;   // 07.5
        hwbyte_t SR1_RXNE           :1;   // 07.6   // Receive Data Register Not Empty
        hwbyte_t SR1_TXE            :1;   // 07.7   // Transmit Data Register Empty
        hwbyte_t SR2_BERR           :1;   // 08.0   // Bus ERRor
        hwbyte_t SR2_ARLO           :1;   // 08.1   // ARbitration LOst in master mode
        hwbyte_t SR2_AF             :1;   // 08.2   // Acknowledge Failure
        hwbyte_t SR2_OVR            :1;   // 08.3   // OVerRun or underrun
        hwbyte_t SR2_PECERR         :1;   // 08.4   // Packet Error Checking ERRor on reception
        hwbyte_t SR2_WUFH           :1;   // 08.5   // Wake-Up From Halt
        hwbyte_t SR2_TIMEOUT        :1;   // 08.6   // TIMEOUT or Tlow error
        hwbyte_t SR2_SMBALERT       :1;   // 08.7   // SMBus ALERT
        hwbyte_t SR3_MSL            :1;   // 09.0   // Master/SLave
        hwbyte_t SR3_BUSY           :1;   // 09.1   // bus BUSY
        hwbyte_t SR3_TRA            :1;   // 09.2   // TRAnsmitter/receiver
        hwbyte_t                    :1;   // 09.3
        hwbyte_t SR3_GENCALL        :1;   // 09.4   // GENeral CALL Header received in slave mode
        hwbyte_t SR3_SMBDEFAULT     :1;   // 09.5   // SMBus DEFAULT address
        hwbyte_t SR3_SMBHOST        :1;   // 09.6   // SMBus HOST header
        hwbyte_t SR3_DUALF          :1;   // 09.7   // DUAL receive address
        hwbyte_t ITR_ITERREN        :1;   // 0A.0   // ERRor InTerrupt ENable
        hwbyte_t ITR_ITEVTEN        :1;   // 0A.1   // EVenT InTerrupt ENable
        hwbyte_t ITR_ITBUFEN        :1;   // 0A.2   // BUFfer InTerrupt ENable
        hwbyte_t ITR_DMAEN          :1;   // 0A.3   // DMA request ENable
        hwbyte_t ITR_LAST           :1;   // 0A.4   // LAST DMA transfer
        hwbyte_t                    :3;   // 0A.5-7
        hwbyte_t                    :8;   // 0B
        hwbyte_t CCRH_CCR           :4;   // 0C.0-3 // Clock Control Register bits [11..8]
        hwbyte_t                    :2;   // 0C.4-5
        hwbyte_t CCRH_DUTY          :1;   // 0C.6   // fast mode DUTY cycle
        hwbyte_t CCRH_FS            :1;   // 0C.7   // Fast/Slow mode
        hwbyte_t TRISER_TRISE       :6;   // 0D.0-5 // maximum RISE Time
        hwbyte_t                    :2;   // 0D.6-7
        hwbyte_t                    :8;   // 0E
    };
    struct {
        hwbyte_t CR1;                     // 00     // Control Register 1
        hwbyte_t CR2;                     // 01     // Control Register 2
        hwbyte_t FREQR;                   // 02     // FREQuency Register
        hwbyte_t OARL;                    // 03     // Own Address Register Low
        hwbyte_t OARH;                    // 04     // Own Address Register High
        hwbyte_t OAR2;                    // 05     // Own Address Register for DUAL mode
        hwbyte_t DR;                      // 06     // Data Register
        hwbyte_t SR1;                     // 07     // Status Register 1
        hwbyte_t SR2;                     // 08     // Status Register 2
        hwbyte_t SR3;                     // 09     // Status Register 3
        hwbyte_t ITR;                     // 0A     // InTerrupt Register
        hwbyte_t CCRL;                    // 0B     // Clock Control Register Low
        hwbyte_t CCRH;                    // 0C     // Clock Control Register High
        hwbyte_t TRISER;                  // 0D     // maximum RISE Time Register
        hwbyte_t PECR;                    // 0E     // Packet Error Checking Register
    };
} I2C_t;
I2C_t I2C1                          @0x5210;
I2C_t I2C                           @0x5210;



// USART section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t SR_PE              :1;   // 00.0   // Parity Error
        hwbyte_t SR_FE              :1;   // 00.1   // Framing Error
        hwbyte_t SR_NF              :1;   // 00.2   // Noise Flag
        hwbyte_t SR_OR              :1;   // 00.3   // OverRun error / LIN Header Error
        hwbyte_t SR_IDLE            :1;   // 00.4   // IDLE line detected
        hwbyte_t SR_RXNE            :1;   // 00.5   // Receive data Register Not Empty
        hwbyte_t SR_TC              :1;   // 00.6   // Transmission Complete
        hwbyte_t SR_TXE             :1;   // 00.7   // Transmit data Register Empty
        hwbyte_t                    :8;   // 01
        hwword_t BRR;                     // 02-03  // Baud Rate Register (strange format)
        hwbyte_t CR1_PIEN           :1;   // 04.0   // Parity error Interrupt ENable
        hwbyte_t CR1_PS             :1;   // 04.1   // Parity Selection
        hwbyte_t CR1_PCEN           :1;   // 04.2   // Parity Control ENable
        hwbyte_t CR1_WAKE           :1;   // 04.3   // WAKE-up method
        hwbyte_t CR1_M              :1;   // 04.4   // word length
        hwbyte_t CR1_USARTD         :1;   // 04.5   // USART Disable
        hwbyte_t CR1_T8             :1;   // 04.6   // Transmit data bit 8
        hwbyte_t CR1_R8             :1;   // 04.7   // Receive data bit 8
        hwbyte_t CR2_SBK            :1;   // 05.0   // Send BreaK
        hwbyte_t CR2_RWU            :1;   // 05.1   // Receiver Wake-Up
        hwbyte_t CR2_REN            :1;   // 05.2   // Receiver ENable
        hwbyte_t CR2_TEN            :1;   // 05.3   // Transmitter ENable
        hwbyte_t CR2_ILIEN          :1;   // 05.4   // Idle Line Interrupt ENable
        hwbyte_t CR2_RIEN           :1;   // 05.5   // Receiver Interrupt ENable
        hwbyte_t CR2_TCIEN          :1;   // 05.6   // Transmission Complete Interrupt ENable
        hwbyte_t CR2_TIEN           :1;   // 05.7   // Transmitter Interrupt ENable
        hwbyte_t CR3_LBCL           :1;   // 06.0   // Last Bit CLock pulse
        hwbyte_t CR3_CPHA           :1;   // 06.1   // Clock PHAse
        hwbyte_t CR3_CPOL           :1;   // 06.2   // Clock POLarity
        hwbyte_t CR3_CLKEN          :1;   // 06.3   // CLocK ENable
        hwbyte_t CR3_STOP           :2;   // 06.4-5 // STOP bits
        hwbyte_t                    :2;   // 06.6-7
        hwbyte_t CR4_ADD            :4;   // 07.0-3 // ADDress of the node
        hwbyte_t                    :4;   // 07.4-7
        hwbyte_t CR5_EIE            :1;   // 08.0   // Error Interrupt Enable
        hwbyte_t CR5_IREN           :1;   // 08.1   // IRda ENable
        hwbyte_t CR5_IRLP           :1;   // 08.2   // IRda Low Power selection
        hwbyte_t CR5_HDSEL          :1;   // 08.3   // Half-Duplex SELection
        hwbyte_t CR5_NACK           :1;   // 08.4   // smart card NACK ENable
        hwbyte_t CR5_SCEN           :1;   // 08.5   // Smart Card mode ENable
        hwbyte_t CR5_DMAR           :1;   // 08.6   // DMA enable Receiver
        hwbyte_t CR5_DMAT           :1;   // 08.7   // DMA enable Transmitter
        hwbyte_t                    :8;   // 09
        hwbyte_t                    :8;   // 0A
    };
    struct {
        hwbyte_t SR;                      // 00     // Status Register
        hwbyte_t DR;                      // 01     // Data Register
        hwbyte_t BRR1;                    // 02     // Baud Rate Register 1
        hwbyte_t BRR2;                    // 03     // Baud Rate Register 2
        hwbyte_t CR1;                     // 04     // Control Register 1
        hwbyte_t CR2;                     // 05     // Control Register 2
        hwbyte_t CR3;                     // 06     // Control Register 3
        hwbyte_t CR4;                     // 07     // Control Register 4
        hwbyte_t CR5;                     // 08     // Control Register 5
        hwbyte_t GTR;                     // 09     // Guard Time Register
        hwbyte_t PSCR;                    // 0A     // Prescaler Register
    };
} USART_t;
USART_t USART1                      @0x5230;
USART_t USART                       @0x5230;



// TIMER 2, 3 section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CR1_CEN            :1;   // 00.0   // Counter Enable
        hwbyte_t CR1_UDIS           :1;   // 00.1   // Update DIsable
        hwbyte_t CR1_URS            :1;   // 00.2   // Update Request Source
        hwbyte_t CR1_OPM            :1;   // 00.3   // One Pulse Mode
        hwbyte_t CR1_DIR            :1;   // 00.4   // count DIRection
        hwbyte_t CR1_CMS            :2;   // 00.5-6 // Center aligned Mode Selection
        hwbyte_t CR1_ARPE           :1;   // 00.7   // Auto-Reload Preload Enable
        hwbyte_t                    :3;   // 01.0-2
        hwbyte_t CR2_CCDS           :1;   // 01.3   // Capture Compare DMA Selection
        hwbyte_t CR2_MMS            :3;   // 01.4-6 // Master Mode Selection
        hwbyte_t CR2_TI1S           :1;   // 01.7   // Timer digital filter Input 1 Selection
        hwbyte_t SMCR_SMS           :3;   // 02.0-2 // Slave Mode Selection
        hwbyte_t                    :1;   // 02.3
        hwbyte_t SMCR_TS            :3;   // 02.4-6 // Trigger Selection
        hwbyte_t SMCR_MSM           :1;   // 02.7   // Master / Slave Mode
        hwbyte_t ETR_ETF            :4;   // 03.0-3 // External Trigger Filter
        hwbyte_t ETR_ETPS           :2;   // 03.4-5 // External Trigger PreScaler
        hwbyte_t ETR_ECE            :1;   // 03.6   // External Clock Enable
        hwbyte_t ETR_ETP            :1;   // 03.7   // External Trigger Polarity
        hwbyte_t DER_UDE            :1;   // 04.0   // Update DMA request Enable
        hwbyte_t DER_CC1DE          :1;   // 04.1   // Capture / Compare 1 DMA request Enable
        hwbyte_t DER_CC2DE          :1;   // 04.2   // Capture / Compare 2 DMA request Enable
        hwbyte_t                    :5;   // 04.3-7
        hwbyte_t IER_UIE            :1;   // 05.0   // Update Interrupt Enable
        hwbyte_t IER_CC1IE          :1;   // 05.1   // Capture/Compare 1 Interrupt Enable
        hwbyte_t IER_CC2IE          :1;   // 05.2   // Capture/Compare 2 Interrupt Enable
        hwbyte_t                    :3;   // 05.3-5
        hwbyte_t IER_TIE            :1;   // 05.6   // Trigger Interrupt Enable
        hwbyte_t IER_BIE            :1;   // 05.7   // Break Interrupt Enable
        hwbyte_t SR1_UIF            :1;   // 06.0   // Update Interrupt Flag
        hwbyte_t SR1_CC1IF          :1;   // 06.1   // Capture/Compare 1 Interrupt Flag
        hwbyte_t SR1_CC2IF          :1;   // 06.2   // Capture/Compare 2 Interrupt Flag
        hwbyte_t                    :3;   // 06.3-5
        hwbyte_t SR1_TIF            :1;   // 06.6   // Trigger Interrupt Flag
        hwbyte_t SR1_BIF            :1;   // 06.7   // Break Interrupt Flag
        hwbyte_t                    :1;   // 07.0
        hwbyte_t SR2_CC1OF          :1;   // 07.1   // Capture/Compare 1 Overcapture Flag
        hwbyte_t SR2_CC2OF          :1;   // 07.2   // Capture/Compare 2 Overcapture Flag
        hwbyte_t                    :5;   // 07.3-7
        hwbyte_t EGR_UG             :1;   // 08.0   // Update Generation
        hwbyte_t EGR_CC1G           :1;   // 08.1   // Capture/Compare 1 Generation
        hwbyte_t EGR_CC2G           :1;   // 08.2   // Capture/Compare 2 Generation
        hwbyte_t                    :3;   // 08.3-5
        hwbyte_t EGR_TG             :1;   // 08.6   // Trigger Generation
        hwbyte_t EGR_BG             :1;   // 08.7   // Break Generation
        hwbyte_t CCMR1_CC1S         :2;   // 09.0-1 // Capture/Compare 1 Selection
        hwbyte_t CCMR1_OC1FE        :1;   // 09.2   // Output Compare 1 Fast Enable
        hwbyte_t CCMR1_OC1PE        :1;   // 09.3   // Output Compare 1 Preload Enable
        hwbyte_t CCMR1_OC1M         :3;   // 09.4-6 // Output Compare 1 Mode
        hwbyte_t                    :1;   // 09.7
        hwbyte_t CCMR2_CC2S         :2;   // 0A.0-1 // Capture/Compare 2 Selection
        hwbyte_t CCMR2_OC2FE        :1;   // 0A.2   // Output Compare 2 Fast Enable
        hwbyte_t CCMR2_OC2PE        :1;   // 0A.3   // Output Compare 2 Preload Enable
        hwbyte_t CCMR2_OC2M         :3;   // 0A.4-6 // Output Compare 2 Mode
        hwbyte_t                    :1;   // 0A.7
        hwbyte_t CCER1_CC1E         :1;   // 0B.0   // Capture/Compare 1 output Enable
        hwbyte_t CCER1_CC1P         :1;   // 0B.1   // Capture/Compare 1 output Polarity
        hwbyte_t                    :2;   // 0B.2-3
        hwbyte_t CCER1_CC2E         :1;   // 0B.4   // Capture/Compare 2 output Enable
        hwbyte_t CCER1_CC2P         :1;   // 0B.5   // Capture/Compare 2 output Polarity
        hwbyte_t                    :2;   // 0B.6-7
        hwword_t CNTR;                    // 0C-0D  // CouNTer Register
        hwbyte_t PSCR_PSC           :3;   // 0E.0-2 // PreSCaler value
        hwbyte_t                    :5;   // 0E.3-7
        hwword_t ARR;                     // 0F-10  // Auto-Reload Register
        hwword_t CCR1;                    // 11-12  // Capture/Compare Register 1
        hwword_t CCR2;                    // 13-14  // Capture/Compare Register 2
        hwbyte_t BKR_LOCK           :2;   // 15.0-1 //
        hwbyte_t BKR_OSSI           :1;   // 15.2   //
        hwbyte_t                    :1;   // 15.3
        hwbyte_t BKR_BKE            :1;   // 15.4   // BreaK Enable
        hwbyte_t BKR_BKP            :1;   // 15.5   // BreaK Polarity
        hwbyte_t BKR_AOE            :1;   // 15.6   // Automatic Output Enable
        hwbyte_t BKR_MOE            :1;   // 15.7   // Main Output Enable
        hwbyte_t OISR_OIS1          :1;   // 16.0   // Output Idle State 1
        hwbyte_t                    :1;   // 16.1
        hwbyte_t OISR_OIS2          :1;   // 16.2   // Output Idle State 2
        hwbyte_t                    :5;   // 16.3-7
    };
    struct {
        hwbyte_t                    :8;   // 00
        hwbyte_t                    :8;   // 01
        hwbyte_t                    :8;   // 02
        hwbyte_t                    :8;   // 03
        hwbyte_t                    :8;   // 04
        hwbyte_t                    :8;   // 05
        hwbyte_t                    :8;   // 06
        hwbyte_t                    :8;   // 07
        hwbyte_t                    :8;   // 08
        hwbyte_t                    :2;   // 09.0-1
        hwbyte_t CCMR1_IC1PSC       :2;   // 09.2-3 // Input Capture 1 PreSCaler
        hwbyte_t CCMR1_IC1F         :4;   // 09.4-7 // Input Capture 1 Filter
        hwbyte_t                    :2;   // 0A.0-1
        hwbyte_t CCMR2_IC2PSC       :2;   // 0A.2-3 // Input Capture 2 PreSCaler
        hwbyte_t CCMR2_IC2F         :4;   // 0A.4-7 // Input Capture 2 Filter
        hwbyte_t                    :8;   // 0B
        hwword_t                    :16;  // 0C-0D
        hwbyte_t                    :8;   // 0E
        hwword_t                    :16;  // 0F-10
        hwword_t                    :16;  // 11-12
        hwword_t                    :16;  // 13-14
        hwbyte_t                    :8;   // 15
        hwbyte_t                    :8;   // 16
    };
    struct {
        hwbyte_t CR1;                     // 00     // Control Register 1
        hwbyte_t CR2;                     // 01     // Control Register 2
        hwbyte_t SMCR;                    // 02     // Slave mode Control Register
        hwbyte_t ETR;                     // 03     // External Trigger Register
        hwbyte_t DER;                     // 03     // DMA1 request Enable Register
        hwbyte_t IER;                     // 03     // Interrupt Enable Register
        hwbyte_t SR1;                     // 04     // Status Register 1
        hwbyte_t SR2;                     // 05     // Status Register 2
        hwbyte_t EGR;                     // 06     // Event Generation Register
        hwbyte_t CCMR1;                   // 07     // Capture/Compare Mode Register 1
        hwbyte_t CCMR2;                   // 08     // Capture/Compare Mode Register 2
        hwbyte_t CCER1;                   // 0A     // Capture/Compare Enable Register 1
        hwbyte_t CNTRH;                   // 0C     // CouNTer Register High
        hwbyte_t CNTRL;                   // 0D     // CouNTer Register Low
        hwbyte_t PSCR;                    // 0E     // PreSCaler Register
        hwbyte_t ARRH;                    // 0F     // Auto-Reload Register High
        hwbyte_t ARRL;                    // 10     // Auto-Reload Register Low
        hwbyte_t CCR1H;                   // 11     // Capture/Compare Register 1 High
        hwbyte_t CCR1L;                   // 12     // Capture/Compare Register 1 Low
        hwbyte_t CCR2H;                   // 13     // Capture/Compare Register 2 High
        hwbyte_t CCR2L;                   // 14     // Capture/Compare Register 2 Low
        hwbyte_t BKR;                     // 15     // BreaK Register
        hwbyte_t OISR;                    // 16     // Output Idle State Register
    };
} TIM2_t;
TIM2_t TIM2                         @0x5250;
TIM2_t TIM3                         @0x5280;



// TIMER 4 section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CR1_CEN            :1;   // 00.0   // Counter Enable
        hwbyte_t CR1_UDIS           :1;   // 00.1   // Update DIsable
        hwbyte_t CR1_URS            :1;   // 00.2   // Update Request Source
        hwbyte_t CR1_OPM            :1;   // 00.3   // One Pulse Mode
        hwbyte_t                    :3;   // 00.4-6
        hwbyte_t CR1_ARPE           :1;   // 00.7   // Auto-Reload Preload Enable
        hwbyte_t                    :4;   // 01.0-3
        hwbyte_t CR2_MMS            :3;   // 01.4-6 // Master Mode Selection
        hwbyte_t                    :1;   // 01.7
        hwbyte_t SMCR_SMS           :3;   // 02.0-2 // Slave Mode Selection
        hwbyte_t                    :1;   // 02.3
        hwbyte_t SMCR_TS            :3;   // 02.4-6 // Trigger Selection
        hwbyte_t SMCR_MSM           :1;   // 02.7   // Master / Slave Mode
        hwbyte_t DER_UDE            :1;   // 03.0   // Update DMA request Enable
        hwbyte_t                    :7;   // 03.1-7
        hwbyte_t IER_UIE            :1;   // 04.0   // Update Interrupt Enable
        hwbyte_t                    :5;   // 04.1-5
        hwbyte_t IER_TIE            :1;   // 04.6   // Trigger Interrupt Enable
        hwbyte_t                    :1;   // 04.7
        hwbyte_t SR1_UIF            :1;   // 05.0   // Update Interrupt Flag
        hwbyte_t                    :5;   // 05.1-5
        hwbyte_t SR1_TIF            :1;   // 05.6   // Trigger Interrupt Flag
        hwbyte_t                    :1;   // 05.7
        hwbyte_t EGR_UG             :1;   // 06.0   // Update Generation
        hwbyte_t                    :5;   // 06.1-5
        hwbyte_t EGR_TG             :1;   // 06.6   // Trigger Generation
        hwbyte_t                    :1;   // 06.7
        hwbyte_t                    :8;   // 07
        hwbyte_t PSCR_PSC           :4;   // 08.0-3 // PreSCaler value
        hwbyte_t                    :4;   // 08.4-7
        hwbyte_t                    :8;   // 09
    };
    struct {
        hwbyte_t CR1;                     // 00     // Control Register 1
        hwbyte_t CR2;                     // 01     // Control Register 2
        hwbyte_t SMCR;                    // 02     // Slave mode Control Register
        hwbyte_t DER;                     // 03     // DMA1 request Enable Register
        hwbyte_t IER;                     // 04     // Interrupt Enable Register
        hwbyte_t SR1;                     // 05     // Status Register 1
        hwbyte_t EGR;                     // 06     // Event Generation Register
        hwbyte_t CNTR;                    // 07     // CouNTer Register
        hwbyte_t PSCR;                    // 08     // PreSCaler Register
        hwbyte_t ARR;                     // 09     // Auto-Reload Register
    };
} TIM4_t;
TIM4_t TIM4                         @0x52E0;



// Infrared section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CR_IREN            :1;   // 00.0   // InfraRed output ENable
        hwbyte_t CR_HSEN            :1;   // 00.1   // High Sink driver ENable
        hwbyte_t                    :6;   // 00.2-7
    };
    struct {
        hwbyte_t CR;                      // 00     // Control Register
    };
} IRTIM_t;
IRTIM_t IRTIM                       @0x52FF;



// ADC section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t CR1_ADON           :1;   // 00.0   // A/D converter On
        hwbyte_t CR1_START          :1;   // 00.1   // START conversion
        hwbyte_t CR1_CONT           :1;   // 00.2   // CONTinuous conversion
        hwbyte_t CR1_EOCIE          :1;   // 00.3   // End Of Conversion Interrupt Enable
        hwbyte_t CR1_AWDIE          :1;   // 00.4   // Analog WatchDog Interrupt Enable
        hwbyte_t CR1_RES            :2;   // 00.5-6 // RESolution
        hwbyte_t CR1_OVERIE         :1;   // 00.7   // OVERrun Interrupt Enable
        hwbyte_t CR2_SMTP1          :3;   // 01.0-2 // SaMPling Time (channels 0..23)
        hwbyte_t CR2_EXTSEL         :2;   // 01.3-4 // EXTernel trigger SELection
        hwbyte_t CR2_TRIGEDGE       :2;   // 01.5-6 // TRIGger EDGE for external triggers
        hwbyte_t CR2_PRESC          :1;   // 01.7   // clock PRESCaler
        hwbyte_t CR3_CHSEL          :5;   // 02.0-4 // CHannel SELect
        hwbyte_t CR3_SMTP2          :3;   // 02.5-7 // SaMPling Time (channel Vrefint)
        hwbyte_t SR_EOC             :1;   // 03.6   // End Of Conversion flag
        hwbyte_t SR_AWD             :1;   // 03.6   // Analog Watch-Dog flag
        hwbyte_t SR_OVER            :1;   // 03.6   // OVERrun flag
        hwbyte_t                    :5;   // 03.3-7
        hwword_t DR;                      // 04-05  // Data Register
        hwword_t HTR;                     // 06-07  // High Threshold Register
        hwword_t LTR;                     // 08-09  // Low Threshold Register
        hwbyte_t                    :4;   // 0A.0-3
        hwbyte_t SQR1_SVREFINT      :1;   // 0A.4   // Select VREFINT for scanning
        hwbyte_t                    :2;   // 0A.5-6
        hwbyte_t SQR1_DMAOFF        :1;   // 0A.7   // DMA disable for a single conversion
        hwbyte_t SQR2_PB2           :1;   // 0B.0   // Analog channel 16 = PB2
        hwbyte_t SQR2_PB1           :1;   // 0B.1   // Analog channel 17 = PB1
        hwbyte_t SQR2_PB0           :1;   // 0B.2   // Analog channel 18 = PB0
        hwbyte_t                    :3;   // 0B.3-5
        hwbyte_t SQR2_PD0           :1;   // 0B.6   // Analog channel 22 = PD0
        hwbyte_t                    :1;   // 0B.7
        hwbyte_t                    :3;   // 0C.0-2
        hwbyte_t SQR3_PB7           :1;   // 0C.3   // Analog channel 11 = PB7
        hwbyte_t SQR3_PB6           :1;   // 0C.4   // Analog channel 12 = PB6
        hwbyte_t SQR3_PB5           :1;   // 0C.5   // Analog channel 13 = PB5
        hwbyte_t SQR3_PB4           :1;   // 0C.6   // Analog channel 14 = PB4
        hwbyte_t SQR3_PB3           :1;   // 0C.7   // Analog channel 15 = PB3
        hwbyte_t                    :4;   // 0D.0-3
        hwbyte_t SQR4_PC4           :1;   // 0D.4   // Analog channel  4 = PC4
        hwbyte_t                    :3;   // 0D.5-7
        hwbyte_t                    :4;   // 0E.0-3
        hwbyte_t TRIGR1_VREFINTON   :1;   // 0E.4   // VREFINT ON
        hwbyte_t                    :3;   // 0E.5-7
        hwbyte_t TRIGR2_PB2         :1;   // 0F.0   // Analog channel 16 = PB2
        hwbyte_t TRIGR2_PB1         :1;   // 0F.1   // Analog channel 17 = PB1
        hwbyte_t TRIGR2_PB0         :1;   // 0F.2   // Analog channel 18 = PB0
        hwbyte_t                    :3;   // 0F.3-5
        hwbyte_t TRIGR2_PD0         :1;   // 0F.6   // Analog channel 22 = PD0
        hwbyte_t                    :1;   // 0F.7
        hwbyte_t                    :3;   // 10.0-2
        hwbyte_t TRIGR3_PB7         :1;   // 10.3   // Analog channel 11 = PB7
        hwbyte_t TRIGR3_PB6         :1;   // 10.4   // Analog channel 12 = PB6
        hwbyte_t TRIGR3_PB5         :1;   // 10.5   // Analog channel 13 = PB5
        hwbyte_t TRIGR3_PB4         :1;   // 10.6   // Analog channel 14 = PB4
        hwbyte_t TRIGR3_PB3         :1;   // 10.7   // Analog channel 15 = PB3
        hwbyte_t                    :4;   // 11.0-3
        hwbyte_t TRIGR4_PC4         :1;   // 11.4   // Analog channel  4 = PC4
        hwbyte_t                    :3;   // 11.5-7
    };
    struct {
        hwbyte_t CR1;                     // 00     // Configuration Register 1
        hwbyte_t CR2;                     // 01     // Configuration Register 2
        hwbyte_t CR3;                     // 02     // Configuration Register 3
        hwbyte_t SR;                      // 03     // Control/Status Register
        hwbyte_t DRH;                     // 04     // Data Register High
        hwbyte_t DRL;                     // 05     // Data Register Low
        hwbyte_t HTRH;                    // 06     // High Threshold Register High
        hwbyte_t HTRL;                    // 07     // High Threshold Register Low
        hwbyte_t LTRH;                    // 08     // Low Threshold Register High
        hwbyte_t LTRL;                    // 09     // Low Threshold Register Low
        hwbyte_t SQR1;                    // 0A     // channel SeQuence Register 1
        hwbyte_t SQR2;                    // 0B     // channel SeQuence Register 2
        hwbyte_t SQR3;                    // 0C     // channel SeQuence Register 3
        hwbyte_t SQR4;                    // 0D     // channel SeQuence Register 4
        hwbyte_t TRIGR1;                  // 0E     // TRIGger disable Register 1
        hwbyte_t TRIGR2;                  // 0F     // TRIGger disable Register 2
        hwbyte_t TRIGR3;                  // 10     // TRIGger disable Register 3
        hwbyte_t TRIGR4;                  // 11     // TRIGger disable Register 4
    };
} ADC_t;
ADC_t ADC1                          @0x5340;
ADC_t ADC                           @0x5340;



// Routing Interface section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t                    :8;   // 00
        hwbyte_t                    :8;   // 01
        hwbyte_t                    :8;   // 02
        hwbyte_t IOIR1_CH1I         :1;   // 03.0   // Channel 1 I/O 1 pin Input value
        hwbyte_t IOIR1_CH4I         :1;   // 03.1   // Channel 1 I/O 4 pin Input value
        hwbyte_t IOIR1_CH7I         :1;   // 03.2   // Channel 1 I/O 7 pin Input value
        hwbyte_t IOIR1_CH10I        :1;   // 03.3   // Channel 1 I/O 10 pin Input value
        hwbyte_t IOIR1_CH13I        :1;   // 03.4   // Channel 1 I/O 13 pin Input value
        hwbyte_t IOIR1_CH16I        :1;   // 03.5   // Channel 1 I/O 16 pin Input value
        hwbyte_t IOIR1_CH19I        :1;   // 03.6   // Channel 1 I/O 19 pin Input value
        hwbyte_t IOIR1_CH22I        :1;   // 03.7   // Channel 1 I/O 22 pin Input value
        hwbyte_t IOIR2_CH2I         :1;   // 04.0   // Channel 2 I/O 2 pin Input value
        hwbyte_t IOIR2_CH5I         :1;   // 04.1   // Channel 2 I/O 5 pin Input value
        hwbyte_t IOIR2_CH8I         :1;   // 04.2   // Channel 2 I/O 8 pin Input value
        hwbyte_t IOIR2_CH11I        :1;   // 04.3   // Channel 2 I/O 11 pin Input value
        hwbyte_t IOIR2_CH14I        :1;   // 04.4   // Channel 2 I/O 14 pin Input value
        hwbyte_t IOIR2_CH17I        :1;   // 04.5   // Channel 2 I/O 17 pin Input value
        hwbyte_t IOIR2_CH20I        :1;   // 04.6   // Channel 2 I/O 20 pin Input value
        hwbyte_t IOIR2_CH23I        :1;   // 04.7   // Channel 2 I/O 23 pin Input value
        hwbyte_t IOIR3_CH3I         :1;   // 05.0   // Channel 3 I/O 3 pin Input value
        hwbyte_t IOIR3_CH6I         :1;   // 05.1   // Channel 3 I/O 6 pin Input value
        hwbyte_t IOIR3_CH9I         :1;   // 05.2   // Channel 3 I/O 9 pin Input value
        hwbyte_t IOIR3_CH12I        :1;   // 05.3   // Channel 3 I/O 12 pin Input value
        hwbyte_t IOIR3_CH15I        :1;   // 05.4   // Channel 3 I/O 15 pin Input value
        hwbyte_t IOIR3_CH18I        :1;   // 05.5   // Channel 3 I/O 18 pin Input value
        hwbyte_t IOIR3_CH21I        :1;   // 05.6   // Channel 3 I/O 21 pin Input value
        hwbyte_t IOIR3_CH24I        :1;   // 05.7   // Channel 3 I/O 24 pin Input value
        hwbyte_t IOCMR1_CH1M        :1;   // 06.0   // Channel 1 I/O 1 pin Control Mode
        hwbyte_t IOCMR1_CH4M        :1;   // 06.1   // Channel 1 I/O 4 pin Control Mode
        hwbyte_t IOCMR1_CH7M        :1;   // 06.2   // Channel 1 I/O 7 pin Control Mode
        hwbyte_t IOCMR1_CH10M       :1;   // 06.3   // Channel 1 I/O 10 pin Control Mode
        hwbyte_t IOCMR1_CH13M       :1;   // 06.4   // Channel 1 I/O 13 pin Control Mode
        hwbyte_t IOCMR1_CH16M       :1;   // 06.5   // Channel 1 I/O 16 pin Control Mode
        hwbyte_t IOCMR1_CH19M       :1;   // 06.6   // Channel 1 I/O 19 pin Control Mode
        hwbyte_t IOCMR1_CH22M       :1;   // 06.7   // Channel 1 I/O 22 pin Control Mode
        hwbyte_t IOCMR2_CH2M        :1;   // 07.0   // Channel 2 I/O 2 pin Control Mode
        hwbyte_t IOCMR2_CH5M        :1;   // 07.1   // Channel 2 I/O 5 pin Control Mode
        hwbyte_t IOCMR2_CH8M        :1;   // 07.2   // Channel 2 I/O 8 pin Control Mode
        hwbyte_t IOCMR2_CH11M       :1;   // 07.3   // Channel 2 I/O 11 pin Control Mode
        hwbyte_t IOCMR2_CH14M       :1;   // 07.4   // Channel 2 I/O 14 pin Control Mode
        hwbyte_t IOCMR2_CH17M       :1;   // 07.5   // Channel 2 I/O 17 pin Control Mode
        hwbyte_t IOCMR2_CH20M       :1;   // 07.6   // Channel 2 I/O 20 pin Control Mode
        hwbyte_t IOCMR2_CH23M       :1;   // 07.7   // Channel 2 I/O 23 pin Control Mode
        hwbyte_t IOCMR3_CH3M        :1;   // 08.0   // Channel 3 I/O 3 pin Control Mode
        hwbyte_t IOCMR3_CH6M        :1;   // 08.1   // Channel 3 I/O 6 pin Control Mode
        hwbyte_t IOCMR3_CH9M        :1;   // 08.2   // Channel 3 I/O 9 pin Control Mode
        hwbyte_t IOCMR3_CH12M       :1;   // 08.3   // Channel 3 I/O 12 pin Control Mode
        hwbyte_t IOCMR3_CH15M       :1;   // 08.4   // Channel 3 I/O 15 pin Control Mode
        hwbyte_t IOCMR3_CH18M       :1;   // 08.5   // Channel 3 I/O 18 pin Control Mode
        hwbyte_t IOCMR3_CH21M       :1;   // 08.6   // Channel 3 I/O 21 pin Control Mode
        hwbyte_t IOCMR3_CH24M       :1;   // 08.7   // Channel 3 I/O 24 pin Control Mode
        hwbyte_t IOSR1_CH1E         :1;   // 09.0   // Channel 1 I/O 1 pin switch control Enable
        hwbyte_t IOSR1_CH4E         :1;   // 09.1   // Channel 1 I/O 4 pin switch control Enable
        hwbyte_t IOSR1_CH7E         :1;   // 09.2   // Channel 1 I/O 7 pin switch control Enable
        hwbyte_t IOSR1_CH10E        :1;   // 09.3   // Channel 1 I/O 10 pin switch control Enable
        hwbyte_t IOSR1_CH13E        :1;   // 09.4   // Channel 1 I/O 13 pin switch control Enable
        hwbyte_t IOSR1_CH16E        :1;   // 09.5   // Channel 1 I/O 16 pin switch control Enable
        hwbyte_t IOSR1_CH19E        :1;   // 09.6   // Channel 1 I/O 19 pin switch control Enable
        hwbyte_t IOSR1_CH22E        :1;   // 09.7   // Channel 1 I/O 22 pin switch control Enable
        hwbyte_t IOSR2_CH2E         :1;   // 0A.0   // Channel 2 I/O 2 pin switch control Enable
        hwbyte_t IOSR2_CH5E         :1;   // 0A.1   // Channel 2 I/O 5 pin switch control Enable
        hwbyte_t IOSR2_CH8E         :1;   // 0A.2   // Channel 2 I/O 8 pin switch control Enable
        hwbyte_t IOSR2_CH11E        :1;   // 0A.3   // Channel 2 I/O 11 pin switch control Enable
        hwbyte_t IOSR2_CH14E        :1;   // 0A.4   // Channel 2 I/O 14 pin switch control Enable
        hwbyte_t IOSR2_CH17E        :1;   // 0A.5   // Channel 2 I/O 17 pin switch control Enable
        hwbyte_t IOSR2_CH20E        :1;   // 0A.6   // Channel 2 I/O 20 pin switch control Enable
        hwbyte_t IOSR2_CH23E        :1;   // 0A.7   // Channel 2 I/O 23 pin switch control Enable
        hwbyte_t IOSR3_CH3E         :1;   // 0B.0   // Channel 3 I/O 3 pin switch control Enable
        hwbyte_t IOSR3_CH6E         :1;   // 0B.1   // Channel 3 I/O 6 pin switch control Enable
        hwbyte_t IOSR3_CH9E         :1;   // 0B.2   // Channel 3 I/O 9 pin switch control Enable
        hwbyte_t IOSR3_CH12E        :1;   // 0B.3   // Channel 3 I/O 12 pin switch control Enable
        hwbyte_t IOSR3_CH15E        :1;   // 0B.4   // Channel 3 I/O 15 pin switch control Enable
        hwbyte_t IOSR3_CH18E        :1;   // 0B.5   // Channel 3 I/O 18 pin switch control Enable
        hwbyte_t IOSR3_CH21E        :1;   // 0B.6   // Channel 3 I/O 21 pin switch control Enable
        hwbyte_t IOSR3_CH24M        :1;   // 0B.7   // Channel 3 I/O 24 pin switch control Enable
        hwbyte_t IOGCR_IOM1         :2;   // 0C.0-1 // I/O Mode 1
        hwbyte_t IOGCR_IOM2         :2;   // 0C.2-3 // I/O Mode 2
        hwbyte_t IOGCR_IOM3         :2;   // 0C.4-5 // I/O Mode 3
        hwbyte_t IOGCR_IOM4         :2;   // 0C.6-7 // I/O Mode 4
        hwbyte_t ASCR1_AS0          :1;   // 0D.0   // Analog Switch 0
        hwbyte_t ASCR1_AS1          :1;   // 0D.1   // Analog Switch 1
        hwbyte_t ASCR1_AS2          :1;   // 0D.2   // Analog Switch 2
        hwbyte_t ASCR1_AS3          :1;   // 0D.3   // Analog Switch 3
        hwbyte_t ASCR1_AS4          :1;   // 0D.4   // Analog Switch 4
        hwbyte_t ASCR1_AS5          :1;   // 0D.5   // Analog Switch 5
        hwbyte_t ASCR1_AS6          :1;   // 0D.6   // Analog Switch 6
        hwbyte_t ASCR1_AS7          :1;   // 0D.7   // Analog Switch 7
        hwbyte_t ASCR1_AS8          :1;   // 0E.0   // Analog Switch 8
        hwbyte_t ASCR1_AS9          :1;   // 0E.1   // Analog Switch 9
        hwbyte_t ASCR1_AS10         :1;   // 0E.2   // Analog Switch 10
        hwbyte_t ASCR1_AS11         :1;   // 0E.3   // Analog Switch 11
        hwbyte_t                    :2;   // 0E.4-5
        hwbyte_t ASCR1_AS14         :1;   // 0E.6   // Analog Switch 14
        hwbyte_t                    :1;   // 0E.7
        hwbyte_t RCR_10KPU          :1;   // 0F.0   // 10k Pull-Up
        hwbyte_t RCR_400KPU         :1;   // 0F.1   // 400k Pull-Up
        hwbyte_t RCR_10KPD          :1;   // 0F.2   // 10k Pull-Down
        hwbyte_t RCR_400KPD         :1;   // 0F.3   // 400k Pull-Down
        hwbyte_t                    :4;   // 0F.4-7
        hwbyte_t                    :8;   // 10
        hwbyte_t                    :8;   // 11
        hwbyte_t                    :8;   // 12
        hwbyte_t                    :8;   // 13
        hwbyte_t                    :8;   // 14
        hwbyte_t                    :8;   // 15
        hwbyte_t                    :8;   // 16
        hwbyte_t                    :8;   // 17
        hwbyte_t                    :8;   // 18
        hwbyte_t                    :8;   // 19
        hwbyte_t                    :8;   // 1A
        hwbyte_t                    :8;   // 1B
        hwbyte_t                    :8;   // 1C
        hwbyte_t                    :8;   // 1D
        hwbyte_t                    :8;   // 1E
        hwbyte_t                    :8;   // 1F
        hwbyte_t CR_TIE             :1;   // 20.0   // Trigger Interrupt Enable
        hwbyte_t CR_TIF             :1;   // 20.1   // Trigger Interrupt Flag
        hwbyte_t CR_AM              :1;   // 20.2  // Acquisition Mode
        hwbyte_t CR_THALT           :1;   // 20.3   // Timer HALT mode
        hwbyte_t                    :4;   // 20.4-7
        hwbyte_t IOMR1_CH1M         :1;   // 21.0   // Channel 1 I/O 1 pin event Mask
        hwbyte_t IOMR1_CH4M         :1;   // 21.1   // Channel 1 I/O 4 pin event Mask
        hwbyte_t IOMR1_CH7M         :1;   // 21.2   // Channel 1 I/O 7 pin event Mask
        hwbyte_t IOMR1_CH10M        :1;   // 21.3   // Channel 1 I/O 10 pin event Mask
        hwbyte_t IOMR1_CH13M        :1;   // 21.4   // Channel 1 I/O 13 pin event Mask
        hwbyte_t IOMR1_CH16M        :1;   // 21.5   // Channel 1 I/O 16 pin event Mask
        hwbyte_t IOMR1_CH19M        :1;   // 21.6   // Channel 1 I/O 19 pin event Mask
        hwbyte_t IOMR1_CH22M        :1;   // 21.7   // Channel 1 I/O 22 pin event Mask
        hwbyte_t IOMR2_CH2M         :1;   // 22.0   // Channel 2 I/O 2 pin event Mask
        hwbyte_t IOMR2_CH5M         :1;   // 22.1   // Channel 2 I/O 5 pin event Mask
        hwbyte_t IOMR2_CH8M         :1;   // 22.2   // Channel 2 I/O 8 pin event Mask
        hwbyte_t IOMR2_CH11M        :1;   // 22.3   // Channel 2 I/O 11 pin event Mask
        hwbyte_t IOMR2_CH14M        :1;   // 22.4   // Channel 2 I/O 14 pin event Mask
        hwbyte_t IOMR2_CH17M        :1;   // 22.5   // Channel 2 I/O 17 pin event Mask
        hwbyte_t IOMR2_CH20M        :1;   // 22.6   // Channel 2 I/O 20 pin event Mask
        hwbyte_t IOMR2_CH23M        :1;   // 22.7   // Channel 2 I/O 23 pin event Mask
        hwbyte_t IOMR3_CH3M         :1;   // 23.0   // Channel 3 I/O 3 pin event Mask
        hwbyte_t IOMR3_CH6M         :1;   // 23.1   // Channel 3 I/O 6 pin event Mask
        hwbyte_t IOMR3_CH9M         :1;   // 23.2   // Channel 3 I/O 9 pin event Mask
        hwbyte_t IOMR3_CH12M        :1;   // 23.3   // Channel 3 I/O 12 pin event Mask
        hwbyte_t IOMR3_CH15M        :1;   // 23.4   // Channel 3 I/O 15 pin event Mask
        hwbyte_t IOMR3_CH18M        :1;   // 23.5   // Channel 3 I/O 18 pin event Mask
        hwbyte_t IOMR3_CH21M        :1;   // 23.6   // Channel 3 I/O 21 pin event Mask
        hwbyte_t IOMR3_CH24M        :1;   // 23.7   // Channel 3 I/O 24 pin event Mask
        hwbyte_t IOMR4_CH29M        :1;   // 24.0   // Channel 4 I/O 29 pin event Mask
        hwbyte_t IOMR4_CH26M        :1;   // 24.1   // Channel 4 I/O 26 pin event Mask
        hwbyte_t                    :4;   // 24.2-5
        hwbyte_t IOMR4_CH27M        :1;   // 24.6   // Channel 4 I/O 27 pin event Mask
        hwbyte_t IOMR4_CH28M        :1;   // 24.7   // Channel 4 I/O 28 pin event Mask
        hwbyte_t IOIR4_CH29I        :1;   // 25.0   // Channel 4 I/O 29 pin Input value
        hwbyte_t IOIR4_CH26I        :1;   // 25.1   // Channel 4 I/O 26 pin Input value
        hwbyte_t                    :4;   // 25.2-5
        hwbyte_t IOIR4_CH27I        :1;   // 25.6   // Channel 4 I/O 27 pin Input value
        hwbyte_t IOIR4_CH28I        :1;   // 25.7   // Channel 4 I/O 28 pin Input value
        hwbyte_t IOCMR4_CH29M       :1;   // 26.0   // Channel 4 I/O 29 pin control Mode
        hwbyte_t IOCMR4_CH26M       :1;   // 26.1   // Channel 4 I/O 26 pin control Mode
        hwbyte_t                    :4;   // 26.2-5
        hwbyte_t IOCMR4_CH27M       :1;   // 26.6   // Channel 4 I/O 27 pin control Mode
        hwbyte_t IOCMR4_CH28M       :1;   // 26.7   // Channel 4 I/O 28 pin control Mode
        hwbyte_t IOSR4_CH29E        :1;   // 27.0   // Channel 4 I/O 29 pin switch control Enable
        hwbyte_t IOSR4_CH26E        :1;   // 27.1   // Channel 4 I/O 26 pin switch control Enable
        hwbyte_t                    :4;   // 27.2-5
        hwbyte_t IOSR4_CH27E        :1;   // 27.6   // Channel 4 I/O 27 pin switch control Enable
        hwbyte_t IOSR4_CH28E        :1;   // 27.7   // Channel 4 I/O 28 pin switch control Enable
    };
    struct {
        hwbyte_t                    :8;   // 00
        hwbyte_t ICR1;                    // 01     // timer Input Capture Register 1
        hwbyte_t ICR2;                    // 02     // timer Input Capture Register 2
        hwbyte_t IOIR1;                   // 03     // I/O Input Register 1
        hwbyte_t IOIR2;                   // 04     // I/O Input Register 2
        hwbyte_t IOIR3;                   // 05     // I/O Input Register 3
        hwbyte_t IOCMR1;                  // 06     // I/O Control Mode Register 1
        hwbyte_t IOCMR2;                  // 07     // I/O Control Mode Register 2
        hwbyte_t IOCMR3;                  // 08     // I/O Control Mode Register 3
        hwbyte_t IOSR1;                   // 09     // I/O Switch Register 1
        hwbyte_t IOSR2;                   // 0A     // I/O Switch Register 2
        hwbyte_t IOSR3;                   // 0B     // I/O Switch Register 3
        hwbyte_t IOGCR;                   // 0C     // I/O Group Control Register
        hwbyte_t ASCR1;                   // 0D     // Analog Switch Register 1
        hwbyte_t ASCR2;                   // 0E     // Analog Switch Register 2
        hwbyte_t RCR;                     // 0F     // Resistor Control Register
        hwbyte_t                    :8;   // 10
        hwbyte_t                    :8;   // 11
        hwbyte_t                    :8;   // 12
        hwbyte_t                    :8;   // 13
        hwbyte_t                    :8;   // 14
        hwbyte_t                    :8;   // 15
        hwbyte_t                    :8;   // 16
        hwbyte_t                    :8;   // 17
        hwbyte_t                    :8;   // 18
        hwbyte_t                    :8;   // 19
        hwbyte_t                    :8;   // 1A
        hwbyte_t                    :8;   // 1B
        hwbyte_t                    :8;   // 1C
        hwbyte_t                    :8;   // 1D
        hwbyte_t                    :8;   // 1E
        hwbyte_t                    :8;   // 1F
        hwbyte_t CR;                      // 20     // Control Register
        hwbyte_t MASKR1;                  // 21     // MASK Register 1
        hwbyte_t MASKR2;                  // 22     // MASK Register 2
        hwbyte_t MASKR3;                  // 23     // MASK Register 3
        hwbyte_t MASKR4;                  // 24     // MASK Register 4
        hwbyte_t IOIR4;                   // 25     // I/O Input Register 4
        hwbyte_t IOCMR4;                  // 26     // I/O Control Mode Register 4
        hwbyte_t IOSR4;                   // 27     // I/O Switch Register 4
    };
} RI_t;
RI_t RI                             @0x5430;



// CFG section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t GCR_SWD            :1;   // 00.0   // SWim Disable
        hwbyte_t GCR_AL             :1;   // 00.1   // Activation Level
        hwbyte_t                    :6;   // 00.2-7
    };
    struct {
        hwbyte_t GCR;                     // 00     // Global Configuration Register
    };
} CFG_t;
CFG_t CFG                           @0x7F60;



// ITC section
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t SPR1_VECT0         :2;   // 00.0-1 VECTor 0 priority
        hwbyte_t SPR1_VECT1         :2;   // 00.2-3 VECTor 1 priority
        hwbyte_t SPR1_VECT2         :2;   // 00.4-5 VECTor 2 priority
        hwbyte_t SPR1_VECT3         :2;   // 00.6-7 VECTor 3 priority
        hwbyte_t SPR2_VECT4         :2;   // 01.0-1 VECTor 4 priority
        hwbyte_t SPR2_VECT5         :2;   // 01.2-3 VECTor 5 priority
        hwbyte_t SPR2_VECT6         :2;   // 01.4-5 VECTor 6 priority
        hwbyte_t SPR2_VECT7         :2;   // 01.6-7 VECTor 7 priority
        hwbyte_t SPR3_VECT8         :2;   // 02.0-1 VECTor 8 priority
        hwbyte_t SPR3_VECT9         :2;   // 02.2-3 VECTor 9 priority
        hwbyte_t SPR3_VECT10        :2;   // 02.4-5 VECTor 10 priority
        hwbyte_t SPR3_VECT11        :2;   // 02.6-7 VECTor 11 priority
        hwbyte_t SPR4_VECT12        :2;   // 03.0-1 VECTor 12 priority
        hwbyte_t SPR4_VECT13        :2;   // 03.2-3 VECTor 13 priority
        hwbyte_t SPR4_VECT14        :2;   // 03.4-5 VECTor 14 priority
        hwbyte_t SPR4_VECT15        :2;   // 03.6-7 VECTor 15 priority
        hwbyte_t SPR5_VECT16        :2;   // 04.0-1 VECTor 16 priority
        hwbyte_t SPR5_VECT17        :2;   // 04.2-3 VECTor 17 priority
        hwbyte_t SPR5_VECT18        :2;   // 04.4-5 VECTor 18 priority
        hwbyte_t SPR5_VECT19        :2;   // 04.6-7 VECTor 19 priority
        hwbyte_t SPR6_VECT20        :2;   // 05.0-1 VECTor 20 priority
        hwbyte_t SPR6_VECT21        :2;   // 05.2-3 VECTor 21 priority
        hwbyte_t SPR6_VECT22        :2;   // 05.4-5 VECTor 22 priority
        hwbyte_t SPR6_VECT23        :2;   // 05.6-7 VECTor 23 priority
        hwbyte_t SPR7_VECT24        :2;   // 07.0-1 VECTor 24 priority
        hwbyte_t SPR7_VECT25        :2;   // 07.2-3 VECTor 25 priority
        hwbyte_t SPR7_VECT26        :2;   // 07.4-5 VECTor 26 priority
        hwbyte_t SPR7_VECT27        :2;   // 07.6-7 VECTor 27 priority
        hwbyte_t SPR8_VECT28        :2;   // 08.0-1 VECTor 28 priority
        hwbyte_t SPR8_VECT29        :2;   // 08.2-3 VECTor 29 priority
        hwbyte_t                    :4;   // 08.4-7
    };
    struct { // Meaningful names
        hwbyte_t VECT_TLI           :2;   // 00.0-1 VECTor 0 priority
        hwbyte_t VECT_FLASH         :2;   // 00.2-3 VECTor 1 priority
        hwbyte_t VECT_DMA1_01       :2;   // 00.4-5 VECTor 2 priority
        hwbyte_t VECT_DMA1_23       :2;   // 00.6-7 VECTor 3 priority
        hwbyte_t VECT_RTC           :2;   // 01.0-1 VECTor 4 priority
        hwbyte_t VECT_PVD           :2;   // 01.2-3 VECTor 5 priority
        hwbyte_t VECT_EXTIB         :2;   // 01.4-5 VECTor 6 priority
        hwbyte_t VECT_EXTID         :2;   // 01.6-7 VECTor 7 priority
        hwbyte_t VECT_EXTI0         :2;   // 02.0-1 VECTor 8 priority
        hwbyte_t VECT_EXTI1         :2;   // 02.2-3 VECTor 9 priority
        hwbyte_t VECT_EXTI2         :2;   // 02.4-5 VECTor 10 priority
        hwbyte_t VECT_EXTI3         :2;   // 02.6-7 VECTor 11 priority
        hwbyte_t VECT_EXTI4         :2;   // 03.0-1 VECTor 12 priority
        hwbyte_t VECT_EXTI5         :2;   // 03.2-3 VECTor 13 priority
        hwbyte_t VECT_EXTI6         :2;   // 03.4-5 VECTor 14 priority
        hwbyte_t VECT_EXTI7         :2;   // 03.6-7 VECTor 15 priority
        hwbyte_t                    :2;   // 04.0-1 VECTor 16 priority
        hwbyte_t VECT_CLK           :2;   // 04.2-3 VECTor 17 priority
        hwbyte_t VECT_ADC           :2;   // 04.4-5 VECTor 18 priority
        hwbyte_t VECT_TIM2_OVF      :2;   // 04.6-7 VECTor 19 priority
        hwbyte_t VECT_TIM2_CAP      :2;   // 05.0-1 VECTor 20 priority
        hwbyte_t VECT_TIM3_OVF      :2;   // 05.2-3 VECTor 21 priority
        hwbyte_t VECT_TIM3_CAP      :2;   // 05.4-5 VECTor 22 priority
        hwbyte_t VECT_RI            :2;   // 05.6-7 VECTor 23 priority
        hwbyte_t                    :2;   // 07.0-1 VECTor 24 priority
        hwbyte_t VECT_TIM4_OVF      :2;   // 07.2-3 VECTor 25 priority
        hwbyte_t VECT_SPI1          :2;   // 07.4-5 VECTor 26 priority
        hwbyte_t VECT_USART1_TX     :2;   // 07.6-7 VECTor 27 priority
        hwbyte_t VECT_USART1_RX     :2;   // 08.0-1 VECTor 28 priority
        hwbyte_t VECT_I2C           :2;   // 08.2-3 VECTor 29 priority
        hwbyte_t                    :4;   // 08.4-7
    };
    struct {
        hwbyte_t SPR1;                    // 00     // Software Priority Register 1
        hwbyte_t SPR2;                    // 01     // Software Priority Register 2
        hwbyte_t SPR3;                    // 02     // Software Priority Register 3
        hwbyte_t SPR4;                    // 03     // Software Priority Register 4
        hwbyte_t SPR5;                    // 04     // Software Priority Register 5
        hwbyte_t SPR6;                    // 05     // Software Priority Register 6
        hwbyte_t SPR7;                    // 06     // Software Priority Register 7
        hwbyte_t SPR8;                    // 07     // Software Priority Register 8
    };
} ITC_t;
ITC_t ITC                           @0x7F70;

// Interrupt priority levels
#define ITRLEVEL1  1   // Low priority
#define ITRLEVEL2  0   // Medium priority
#define ITRLEVEL3  3   // High priority



// Option bytes
//-------------------------------------------------------------------------
typedef volatile union {
    struct {
        hwbyte_t OPT0_ROP;                // 00     // Read-Out Protection
        hwbyte_t                    :8;   // 01
        hwbyte_t OPT1_UBC;                // 02     // User Boot Code size
        hwbyte_t                    :8;   // 03
        hwbyte_t                    :8;   // 04
        hwbyte_t                    :8;   // 05
        hwbyte_t                    :8;   // 06
        hwbyte_t                    :8;   // 07
        hwbyte_t OPT3_IWDGHW        :1;   // 08.0   // Independent WatchDog activate by HW
        hwbyte_t OPT3_IWDGHALT      :1;   // 08.1   // Independent WatchDog off in (active) HALT
        hwbyte_t OPT3_WWDGHW        :1;   // 08.2   // Window WatchDog activate by HW
        hwbyte_t OPT3_WWDGHALT      :1;   // 08.3   // Window WatchDog off in (active) HALT
        hwbyte_t                    :4;   // 08.4-7
        hwbyte_t OPT4_HSECNT        :2;   // 09.0-1 // High Speed External oscillator stabilization CouNT
        hwbyte_t OPT4_LSECNT        :2;   // 09.2-3 // Low Speed External oscillator stabilization CouNT
        hwbyte_t                    :4;   // 09.4-7
        hwbyte_t OPT5_BORON         :1;   // 0A.0   // BrownOut Reset ON
        hwbyte_t OPT5_BORTH         :3;   // 0A.1-3 // BrownOut Reset THreshold
        hwbyte_t                    :4;   // 0A.4-7
        hwword_t OPTBL;                   // 0B-0C
    };
    struct {
        hwbyte_t OPT0;                    // 00
        hwbyte_t                    :8;   // 01
        hwbyte_t OPT1;                    // 02
        hwbyte_t                    :8;   // 03
        hwbyte_t                    :8;   // 04
        hwbyte_t                    :8;   // 05
        hwbyte_t                    :8;   // 06
        hwbyte_t                    :8;   // 07
        hwbyte_t OPT3;                    // 08
        hwbyte_t OPT4;                    // 09
        hwbyte_t OPT5;                    // 0A
        hwword_t                    :16;  // 0B-0C
    };
} OPT_t;
OPT_t OPT                           @0x4800;



// Memory areas
//-------------------------------------------------------------------------
const hwbyte_t ROM    [0x2000]      @0x8000;
const hwbyte_t EEPROM [0x0100]      @0x1000;
      hwbyte_t RAM    [0x0400]      @0x0000;



#endif
