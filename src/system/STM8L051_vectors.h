#define __STM8L051_vectors_h

/**
 * @file
 * Standard vectors table for STM8L051.
 ******************************************************************************/


/**
 * Standard vectors table.
 * Do not modify!
 ******************************************************************************/
#pragma section const {vectors}
void (* const @vector _vectab[32])() = {
    VECTOR_RESET     ,   //    RESET
    VECTOR_TRAP      ,   //    TRAP
    VECTOR_TLI       ,   //  0 TLI
    VECTOR_FLASH     ,   //  1 FLASH
    VECTOR_DMA1_01   ,   //  2 DMA1_01
    VECTOR_DMA1_23   ,   //  3 DMA1_23
    VECTOR_RTC       ,   //  4 RTC
    VECTOR_PVD       ,   //  5 PVD
    VECTOR_EXTIB     ,   //  6 EXTIB
    VECTOR_EXTID     ,   //  7 EXTID
    VECTOR_EXTI0     ,   //  8 EXTI0
    VECTOR_EXTI1     ,   //  9 EXTI1
    VECTOR_EXTI2     ,   // 10 EXTI2
    VECTOR_EXTI3     ,   // 11 EXTI3
    VECTOR_EXTI4     ,   // 12 EXTI4
    VECTOR_EXTI5     ,   // 13 EXTI5
    VECTOR_EXTI6     ,   // 14 EXTI6
    VECTOR_EXTI7     ,   // 15 EXTI7
    VECTOR_RESERVED  ,   // 16 Reserved
    VECTOR_CLK       ,   // 17 CLK
    VECTOR_ADC       ,   // 18 ADC
    VECTOR_TIM2_OVF  ,   // 19 TIM2_OVF
    VECTOR_TIM2_CAP  ,   // 20 TIM2_CAP
    VECTOR_TIM3_OVF  ,   // 21 TIM3_OVF
    VECTOR_TIM3_CAP  ,   // 22 TIM3_CAP
    VECTOR_RI        ,   // 23 RI
    VECTOR_RESERVED  ,   // 24 Reserved
    VECTOR_TIM4_OVF  ,   // 25 TIM4_OVF
    VECTOR_SPI1      ,   // 26 SPI1
    VECTOR_USART1_TX ,   // 27 USART1_TX
    VECTOR_USART1_RX ,   // 28 USART1_RX
    VECTOR_I2C       ,   // 29 I2C
};
#pragma section const {}
