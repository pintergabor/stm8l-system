#ifndef __typedefs0_h
#define __typedefs0_h

/**
 * @file
 * Common types for byte level and higher access.
 * ST8 is big-endian.
 ******************************************************************************/


// 8-bit
//------------------------------
typedef signed char int8;
typedef unsigned char uint8;
typedef unsigned char byte;


// 16-bit
//------------------------------
typedef signed short int16;
typedef unsigned short uint16;
typedef unsigned short word;

// Access to high and low bytes of variables
#define HI(w)   (((byte *)(&(w)))[0])
#define LO(w)   (((byte *)(&(w)))[1])

// Access to high and low bytes of constants
#define HIGH(w) ((byte)(((w)>>8)&0xFF))
#define LOW(w)  ((byte)(((w)   )&0xFF))


// 32-bit
//------------------------------
typedef signed long int32;
typedef unsigned long uint32;
typedef unsigned long dword;


//------------------------------
// Simple callback
typedef void (*callback)(void);


#endif
