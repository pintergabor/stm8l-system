#ifndef __mods0_h
#define __mods0_h

/**
 * @file
 * Short stack model override.
 ******************************************************************************/


// Default placement of data
#pragma space       [] @near
#pragma space auto  [] @tiny
#pragma space const [] @near

// Default placement of code
#pragma space       () @near


#endif
