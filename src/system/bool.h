#ifndef __bool_h
#define __bool_h

/**
 * @file
 * Boolean type true and false.
 ******************************************************************************/


// 1-bit
//------------------------------
typedef _Bool bit;
#define TRUE  1
#define FALSE 0


// 8-bit
//------------------------------
typedef unsigned char bool;
#define true  '\x01'
#define false '\x00'


#endif
