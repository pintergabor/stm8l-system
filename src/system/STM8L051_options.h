#define __STM8L051_options_h

/**
 * @file
 * Standard options table for STM8L051
 ******************************************************************************/


/**
 * Standard options table
 * Do not modify!
 * In debug mode options must be set in STVD, because STVD is stupid.
 ******************************************************************************/
#ifndef __DEBUG__
#pragma section const {opt0}
const byte _opt0= OPT0;
#pragma section const {opt1}
const byte _opt1= OPT1;
#pragma section const {opt2}
const byte _opt2= OPT2;
#pragma section const {opt3}
const byte _opt3= OPT3;
#pragma section const {opt4}
const byte _opt4= OPT4;
#pragma section const {opt5}
const byte _opt5= OPT5;
#pragma section const {optbl}
const word _optbl= OPTBL;
#pragma section const {}
#endif
