#ifndef __typedefs1_h
#define __typedefs1_h

/**
 * @file
 * Common types for bit level access.
 * ST8 is big-endian.
 ******************************************************************************/


// 8-bit
//------------------------------
typedef union {
    struct {
        unsigned D0:1;
        unsigned D1:1;
        unsigned D2:1;
        unsigned D3:1;
        unsigned D4:1;
        unsigned D5:1;
        unsigned D6:1;
        unsigned D7:1;
    };
    byte b[1];
} BYTE;


// 16-bit
//------------------------------
typedef union {
    struct {
        unsigned D8:1;
        unsigned D9:1;
        unsigned D10:1;
        unsigned D11:1;
        unsigned D12:1;
        unsigned D13:1;
        unsigned D14:1;
        unsigned D15:1;

        unsigned D0:1;
        unsigned D1:1;
        unsigned D2:1;
        unsigned D3:1;
        unsigned D4:1;
        unsigned D5:1;
        unsigned D6:1;
        unsigned D7:1;
    };
    byte b[2];
    word w[1];
} WORD;


// 32-bit
//------------------------------
typedef union {
    struct {
        unsigned D24:1;
        unsigned D25:1;
        unsigned D26:1;
        unsigned D27:1;
        unsigned D28:1;
        unsigned D29:1;
        unsigned D30:1;
        unsigned D31:1;

        unsigned D16:1;
        unsigned D17:1;
        unsigned D18:1;
        unsigned D19:1;
        unsigned D20:1;
        unsigned D21:1;
        unsigned D22:1;
        unsigned D23:1;

        unsigned D8:1;
        unsigned D9:1;
        unsigned D10:1;
        unsigned D11:1;
        unsigned D12:1;
        unsigned D13:1;
        unsigned D14:1;
        unsigned D15:1;

        unsigned D0:1;
        unsigned D1:1;
        unsigned D2:1;
        unsigned D3:1;
        unsigned D4:1;
        unsigned D5:1;
        unsigned D6:1;
        unsigned D7:1;
    };
    byte b[4];
    word w[2];
    dword dw[1];
} DWORD;


#endif
