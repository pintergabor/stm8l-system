#ifndef __vectors_h
#define __vectors_h

/**
 * @file
 * Vectors.
 ******************************************************************************/


/**
 * A true vector.
 *
 * 0x82, 0x00, MSB, LSB
 ******************************************************************************/
typedef const struct {
    byte _x82;
    byte _x00;
    word vector;
} hwvector_t;
#define HWVECTOR(addr)  { 0x82, 0x00, (word)(addr) }

/**
 * A relocated or soft vector.
 *
 * 0xCC, MSB, LSB, 0x9D
 ******************************************************************************/
typedef const struct {
    byte _xCC;
    word vector;
    byte _x9D;
} swvector_t;
#define SWVECTOR(addr)  { 0xCC, (word)(addr), 0x9D }


#endif
