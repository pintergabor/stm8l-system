#ifndef __wait_h
#define __wait_h

/**
 * @file
 * Simple waits.
 ******************************************************************************/


/**
 * Wait 0/1/2/3/4/5/6/7/8 cycles
 ******************************************************************************/
@inline void wait0(void) {                   }
@inline void wait1(void) { _asm("nop");      }
@inline void wait2(void) { wait1(); wait1(); }
@inline void wait3(void) { wait2(); wait1(); }
@inline void wait4(void) { wait3(); wait1(); }
@inline void wait5(void) { wait4(); wait1(); }
@inline void wait6(void) { wait5(); wait1(); }
@inline void wait7(void) { wait6(); wait1(); }
@inline void wait8(void) { wait7(); wait1(); }


/**
 * General purpose wait
 * Wait (3*cycles) cycles, if executed in flash memory
 * 1 <= cycles <= 0xFFFF
 * cycles=0 waits 196608 cycles, which is 12.288ms @16MHz
 ******************************************************************************/
@inline static void waitc(word cycles) {
    _asm("", cycles);
    _asm("$N: decw X");
    _asm("    jrne $L");
}


/**
 * Wait approximately us microseconds, if the clock is xtal Hz
 ******************************************************************************/
#define waitus(us, xtal) (waitc((word)(((double)(us)/1E6*(xtal)+0.5)/3)))


/**
 * General purpose long wait
 * Wait (770*cycles-1) cycles, if executed in flash memory
 * 1 <= cycles <= 0xFFFF
 * cycles=0 waits 50462719 cycles, which is 3.15392s @16MHz
 ******************************************************************************/
@inline static void waitxc(word cycles) {
    _asm("", cycles);
    _asm("    clr A");
    _asm("$N: dec A");
    _asm("    jrne $L");
    _asm("    decw X");
    _asm("    jrne $L");
}


/**
 * Wait approximately ms milliseconds, if the clock is xtal Hz
 ******************************************************************************/
#define waitms(ms, xtal) (waitxc((word)(((double)(ms)/1E3*(xtal)-0.5)/770)))


#endif
