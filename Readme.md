# STM8L processor definitions and useful macros
Created from various example files by  
Pintér Gábor <pinter.gabor@gmx.at>  
https://pintergabor.eu  
Please VERIFY before use!

To the best of my knowledge I did not use any copyrighted material in creation of these files
and I explicitly waive all claims of copyright (economic and moral) of my work,
and immediately place it in the public domain; it may be used, modified or distributed
in any manner whatsoever without further attribution or notice to me.

However if you keep my name, email address and website address intact in this file,
you, and those who use the files after you, may receive some support,
in case you find errors in them, and if you have questions, I may answer them.

## For STM8L051
 * STM8L051

## General
 * bool.h  
   Boolean type that exists in many C variants.
 * typedef0.h  
   Common types for byte level and higher access.
 * typedef1.h  
   Common types for bit level access.
 * typedef.h  
   Combined typedef0.h and typedef1.h.
 * inline.h  
   Assortment of useful macros.
 * vector.h  
   Macros to create vector tables.
 * wait.h  
   Simple waits.
 * mods0.h  
   Short stack model, most suitable for these small processors.
